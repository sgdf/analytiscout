import { Component, OnInit, inject } from '@angular/core';
import { TranslateDirective } from 'app/shared/language';

import { AccountService } from 'app/core/auth/account.service';
import { IVersion, Version } from 'app/entities/version/version.model';
import { VersionService } from 'app/entities/version/version.service';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  standalone: true,
  selector: 'jhi-footer',
  templateUrl: './footer.component.html',
  imports: [TranslateDirective],
  styleUrls: ['./footer.component.scss'],
})
export default class FooterComponent implements OnInit {
  version: IVersion = new Version(0, 0, 0, '', '');
  afficherMobile = true;

  private versionService = inject(VersionService);

  constructor(private accountService: AccountService,
    private deviceService: DeviceDetectorService) {
    this.afficherMobile = this.deviceService.isMobile();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  public afficheVersion(): string {
    if (this.version.mode && this.version.mode.length > 0) {
      return (
        String(this.version.major) +
        '.' +
        String(this.version.minor) +
        '.' +
        String(this.version.subMinor) +
        ' - ' +
        String(this.version.mode) +
        ' - ' +
        String(this.version.buildDate)
      );
    }
    return (
      String(this.version.major) +
      '.' +
      String(this.version.minor) +
      '.' +
      String(this.version.subMinor) +
      ' - ' +
      String(this.version.buildDate)
    );
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  private loadAll(): void {
    this.versionService.get().subscribe(version => {
      this.version = version;
    });
  }
}
