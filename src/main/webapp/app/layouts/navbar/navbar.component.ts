import { Component, Inject, inject, signal, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';

import { StateStorageService } from 'app/core/auth/state-storage.service';
import SharedModule from 'app/shared/shared.module';
import HasAnyAuthorityDirective from 'app/shared/auth/has-any-authority.directive';
import { VERSION } from 'app/app.constants';
import { LANGUAGES } from 'app/config/language.constants';
import { Account } from 'app/core/auth/account.model';
import { AccountService } from 'app/core/auth/account.service';
import { LoginService } from 'app/login/login.service';
import { ProfileService } from 'app/layouts/profiles/profile.service';
import { EntityNavbarItems } from 'app/entities/entity-navbar-items';
import ActiveMenuDirective from './active-menu.directive';
import NavbarItem from './navbar-item.model';
import { IVersion, Version } from 'app/entities/version/version.model';
import { VersionService } from 'app/entities/version/version.service';
import { ANALYTISCOUT_STORAGE_SELECTED_STRUCTURES, ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS, ANALYTISCOUT_STORAGE_STRUCTURE_CHARGEES } from '../../app.constants'

@Component({
  standalone: true,
  selector: 'jhi-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss',
  imports: [RouterModule, SharedModule, HasAnyAuthorityDirective, ActiveMenuDirective],
})
export default class NavbarComponent implements OnInit {
  inProduction?: boolean;
  isNavbarCollapsed = signal(true);
  languages = LANGUAGES;
  openAPIEnabled?: boolean;
  version = '';
  versionProduct: IVersion = new Version(0, 0, 0, "", "");
  account = inject(AccountService).trackCurrentAccount();
  entitiesNavbarItems: NavbarItem[] = [];

  private loginService = inject(LoginService);
  private translateService = inject(TranslateService);
  private stateStorageService = inject(StateStorageService);
  private profileService = inject(ProfileService);
  private router = inject(Router);
  private accountService = inject(AccountService);
  private versionService = inject(VersionService);

  constructor(@Inject(SESSION_STORAGE) private sessionStorageService: StorageService) {
    if (VERSION) {
      this.version = VERSION.toLowerCase().startsWith('v') ? VERSION : `v${VERSION}`;
    }
  }

  ngOnInit(): void {
    this.entitiesNavbarItems = EntityNavbarItems;
    this.profileService.getProfileInfo().subscribe(profileInfo => {
      this.inProduction = profileInfo.inProduction;
      this.openAPIEnabled = profileInfo.openAPIEnabled;
    });
    this.versionService.get().subscribe(version => {
      this.versionProduct = version;
    });
  }

  versionDev(): boolean {
    if (this.versionProduct.mode?.includes("SNAPSHOT")) { return true };
    if (this.versionProduct.mode?.includes("DEV")) { return true };
    if (this.versionProduct.mode?.includes("dev")) { return true };
    return false;
  }

  changeLanguage(languageKey: string): void {
    this.stateStorageService.storeLocale(languageKey);
    this.translateService.use(languageKey);
  }

  collapseNavbar(): void {
    this.isNavbarCollapsed.set(true);
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  isWork(): boolean {
    return this.versionProduct.work!;
  }

  login(): void {
    this.loginService.login();
  }

  displayFonction(): string {
    const structureFonctions = this.sessionStorageService.get(ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS);

    const adherent: any = this.accountService.adherent();
    const fonctions: any[] = adherent.fonctions;
    for (const fonction of fonctions) {
      if (fonction.codeStructure === structureFonctions.codeStructure) {
        return String(fonction.nom) + " - " + String(structureFonctions.nomStructure);
      }
    };


    return String(structureFonctions.nomStructure);
  }

  showJeunes(): boolean {
    const structureChargees = this.sessionStorageService.get(ANALYTISCOUT_STORAGE_STRUCTURE_CHARGEES);
    if (structureChargees !== undefined) {
      return true;
    }
    const structureFonctions = this.sessionStorageService.get(ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS);
    if (structureFonctions.echelonStructure === "National") {
      return false;
    }
    if (structureFonctions.codeStructure === 0) {
      return false;
    }
    return true;
  }

  switchStructure(structure: any): void {
    this.sessionStorageService.remove(ANALYTISCOUT_STORAGE_STRUCTURE_CHARGEES);
    this.sessionStorageService.remove(ANALYTISCOUT_STORAGE_SELECTED_STRUCTURES);
    this.sessionStorageService.set(ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS, structure);
    const currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  logout(): void {
    this.collapseNavbar();
    this.loginService.logout();
    this.router.navigate(['']);
  }

  toggleNavbar(): void {
    this.isNavbarCollapsed.update(isNavbarCollapsed => !isNavbarCollapsed);
  }
}
