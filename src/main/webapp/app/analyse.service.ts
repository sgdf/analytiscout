import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AnalyseService {
  public resourceUrl = '/api/analytiscout';

  constructor(protected http: HttpClient) { }

  getResponsables(structures : any[]): Observable<any> {
    return this.http.post<any>(`${this.resourceUrl}/responsables`, {structures}).pipe(timeout(300000));
  }

  getJeunes(structures : any[]): Observable<any> {
    return this.http.post<any>(`${this.resourceUrl}/jeunes`, {structures}).pipe(timeout(300000));
  }

  getMarins(structures : any[]): Observable<any> {
    return this.http.post<any>(`${this.resourceUrl}/marins`, {structures}).pipe(timeout(300000));
  }

  getStructures(structureFonctions: any): Observable<any> {
    return this.http.post<any>(`${this.resourceUrl}/structures/structures`, {structureFonctions});
  }

  getStructuresHie(jeunes: boolean, structureFonctions: any): Observable<any> {
    const sjeunes = String(jeunes);
    return this.http.post<any>(`${this.resourceUrl}/structures/structuresHie/${sjeunes}`, {structureFonctions});
  }

  getStructureHie(code: string): Observable<any> {
    return this.http.get<any>(`${this.resourceUrl}/structures/structureHie/${code}`);
  }

  getStructureHieFull(code: string): Observable<any> {
    return this.http.get<any>(`${this.resourceUrl}/structures/structureHie/${code}/full`);
  }

  getAdminSessions(): Observable<any> {
    return this.http.get<any>(`${this.resourceUrl}/admin/sessions`);
  }

  getAdminApplicativeSessions(): Observable<any> {
    return this.http.get<any>(`${this.resourceUrl}/admin/applicativeSessions`);
  }
}
