import { IUser } from './user.model';

export const sampleWithRequiredData: IUser = {
  id: '3eedb8b9-4dba-4a26-9f8f-a3335ed1ef63',
  login: "?4jB_@f\\'8yyW\\xg92O\\s9QRBpd\\vBT",
};

export const sampleWithPartialData: IUser = {
  id: '5d5eb926-a61b-4bd7-9e81-02ca965b8d1c',
  login: 'nX@DOb',
};

export const sampleWithFullData: IUser = {
  id: 'aacdf100-26df-4f1a-9000-732ebff2e703',
  login: 'c^o@JBvFe\\y95c',
};
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
