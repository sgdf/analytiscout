import { IAuthority, NewAuthority } from './authority.model';

export const sampleWithRequiredData: IAuthority = {
  name: '3151660e-dfe9-4160-988c-71bc29646d45',
};

export const sampleWithPartialData: IAuthority = {
  name: '5609bd24-e51d-4360-8099-c2c99600b55f',
};

export const sampleWithFullData: IAuthority = {
  name: '95580b7c-fa5d-4515-9808-5030b98af893',
};

export const sampleWithNewData: NewAuthority = {
  name: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
