import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IVersion } from 'app/entities/version/version.model';

@Injectable({ providedIn: 'root' })
export class VersionService {
  public resourceUrl = SERVER_API_URL + 'api/analytiscout/infos';

  constructor(protected http: HttpClient) {}

  get(): Observable<IVersion> {
    return this.http.get<IVersion>(`${this.resourceUrl}/version`);
  }
}
