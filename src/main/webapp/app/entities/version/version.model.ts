export interface IVersion {
    major?: number;
    minor?: number;
    subMinor?: number;
    mode?: string;
    buildDate? : string;
    work?: boolean;
    workMessage? : string;
  }
  
  export class Version implements IVersion {
    constructor(public major?: number, public minor?: number, public subMinor?: number, public mode?: string, public buildDate?: string, public work?: boolean, public workMessage?: string) {}
  }