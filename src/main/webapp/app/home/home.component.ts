import { Component, Inject, inject, signal, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { LoginService } from 'app/login/login.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';
import { ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS } from '../app.constants';

import { IVersion, Version } from 'app/entities/version/version.model';
import { VersionService } from 'app/entities/version/version.service';

@Component({
  standalone: true,
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
  imports: [SharedModule, RouterModule],
})
export default class HomeComponent implements OnInit {
  account = signal<Account | null>(null);
  selectFonction: any = undefined;
  version: IVersion = new Version(0, 0, 0, '', '', false, '');

  private accountService = inject(AccountService);
  private loginService = inject(LoginService);
  private versionService = inject(VersionService);

  ngOnInit(): void {
    this.accountService.identity().subscribe(account => this.account.set(account));
    this.loadAll();
  }

  afficheAdherent(): string {
    if (this.account() !== null && this.account()!.adherent !== undefined) {
      return String(this.account()!.adherent.nom) + ' ' + String(this.account()!.adherent.prenom);
    }
    return this.account() !== null ? this.account()!.login : '';
  }

  afficheFonction(): string {
    const structureFonctionsj = sessionStorage.getItem(ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS);
    if (structureFonctionsj !== null) {
      const structureFonctions = JSON.parse(structureFonctionsj);
      if (this.account() !== null && this.account()!.adherent !== undefined) {
        const structures = this.account()!.adherent.fonctions;
        for (const structure of structures) {
          if (structureFonctions === undefined || structure.codeStructure === structureFonctions.codeStructure) {
            return String(structure.nom);
          }
        }
        return '';
      }
    }
    return '';
  }

  afficheStructure(): string {
    const structureFonctionsj = sessionStorage.getItem(ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS);
    if (structureFonctionsj !== null) {
      const structureFonctions = JSON.parse(structureFonctionsj);
      if (this.account() !== null && this.account()!.adherent !== undefined) {
        const structures = this.account()!.adherent.fonctions;
        for (const structure of structures) {
          if (structureFonctions === undefined || structure.codeStructure === structureFonctions.codeStructure) {
            return String(structure.nomStructure);
          }
        }
        return '';
      }
    }
    return '';
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  isWork(): boolean {
    return this.version.work!;
  }

  login(): void {
    this.loginService.login();
  }

  private loadAll(): void {
    this.versionService.get().subscribe(version => {
      this.version = version;
    });
  }
}
