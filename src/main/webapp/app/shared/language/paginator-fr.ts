import { MatPaginatorIntl } from '@angular/material/paginator';

const rangeLabel = (page: number, pageSize: number, length: number) : string => {
  if (length === 0 || pageSize === 0) { return `Éléments 0 sur ${length}`; }
  
  length = Math.max(length, 0);

  const startIndex = page * pageSize;

  // If the start index exceeds the list length, do not try and fix the end index to the end.
  const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;

  return `Éléments ${startIndex + 1} - ${endIndex} sur ${length}`;
}

export function getFrenchPaginatorIntl() : MatPaginatorIntl {
  const paginatorIntl = new MatPaginatorIntl();
  
  paginatorIntl.itemsPerPageLabel = 'Éléments par page :';
  paginatorIntl.nextPageLabel = 'Page suivante';
  paginatorIntl.previousPageLabel = 'Page Précédente';
  paginatorIntl.getRangeLabel = rangeLabel;
  
  return paginatorIntl;
}