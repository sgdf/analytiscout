import { Routes } from '@angular/router';

import { ResponsablesComponent } from './responsables.component';

const responsablesRoutes: Routes = [
  {
    path: '',
    component: ResponsablesComponent,
    title: 'pages.responsables'
  },
  {
    path: ':id',
    component: ResponsablesComponent,
    title: 'pages.responsables'
  }
]

export default responsablesRoutes;
