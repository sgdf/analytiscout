import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MessageService } from 'primeng/api';

import SharedModule from 'app/shared/shared.module';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { NgxUiLoaderModule, NgxUiLoaderService } from 'ngx-ui-loader';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS } from '../../app.constants';

import { AnalyseComponent } from '../analyse.component';
import { AnalyseService } from '../../analyse.service';
import ToFixedPipe from 'app/shared/tofixed.pipe';
import { FormsModule } from '@angular/forms';

@Component({
  standalone: true,
  selector: 'jhi-responsables',
  templateUrl: './responsables.component.html',
  styleUrls: ['./responsables.component.scss', '../analyse.component.scss'],
  providers: [MessageService],
  imports: [
    SharedModule,
    NgxUiLoaderModule,
    ToFixedPipe,
    FormsModule 
  ],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ResponsablesComponent extends AnalyseComponent implements OnInit {
  values: any = [];
  valuesCompas: any = [];
  valuesFarfadets: any = [];
  valuesAlertes: any = [];
  valuesQuotaParUnite: any = [];
  valuesQuotaAnnee: any = [];
  valuesSyntheseParUnite: any = [];
  dataSource: MatTableDataSource<any> = new MatTableDataSource(this.values);
  dataSourceFqd: MatTableDataSource<any> = new MatTableDataSource(this.values);
  dataSourceDivers: MatTableDataSource<any> = new MatTableDataSource(this.values);
  dataSourceMarins: MatTableDataSource<any> = new MatTableDataSource(this.values);
  dataSourceDroits: MatTableDataSource<any> = new MatTableDataSource(this.values);
  dataSourceCompas: MatTableDataSource<any> = new MatTableDataSource(this.valuesCompas);
  dataSourceFarfadets: MatTableDataSource<any> = new MatTableDataSource(this.valuesFarfadets);
  dataSourceAlertes: MatTableDataSource<any> = new MatTableDataSource(this.valuesAlertes);
  dataSourceSyntheseParUnite: MatTableDataSource<any> = new MatTableDataSource(this.valuesSyntheseParUnite);
  dataSourceQuotaParUnite: MatTableDataSource<any> = new MatTableDataSource(this.valuesQuotaParUnite);
  dataSourceQuotaAnnee: MatTableDataSource<any> = new MatTableDataSource(this.valuesQuotaAnnee);
  displayedColumns: string[] = [
    'status',
    'codeAdherent',
    'nom',
    'prenom',
    'nomStructure',
    'codeGroupe',
    'codeStructure',
    'fonction',
    'fonctionSecondaire',
    'age',
    'ageDirecteur',
    'age18ans1Juillet',
  ];
  displayedColumnsFqd: string[] = [
    'codeAdherent',
    'nom',
    'prenom',
    'fonction',
    'qualificationsDir',
    'qualificationsAnim',
    'bafapotentiel',
    'diplomes',
    'formation_apf',
    'formation_tech',
    'formation_appro',
    'actions',
  ];
  displayedColumnsFormationsDiverses: string[] = ['codeAdherent', 'nom', 'prenom', 'fonction', 'formation_elam' ,'formation_ebienvenue'];
  displayedColumnsfarfadets: string[] = ['codeAdherent', 'nom', 'prenom', 'fonction', 'diplomes', 'formations'];
  displayedColumnsCompas: string[] = ['codeAdherent', 'nom', 'prenom', 'fonction', 'fonctionSecondaire', 'diplomes', 'formations'];
  displayedColumnsAlertes: string[] = [
    'codeAdherent',
    'nom',
    'prenom',
    'nomStructure',
    'codeGroupe',
    'codeStructure',
    'branche',
    'severite',
    'type',
    'alerte',
  ];
  displayedColumnsSyntheseParUnite: string[] = [
    'unite',
    'codeGroupe',
    'codeStructure',
    'branche',
    'jeunes',
    'chefs',
    'ratio',
    'dirsf',
    'animsf',
    'apf',
    'tech',
    'appro',
    'psc1',
    'actions',
  ];
  displayedColumnsQuotaAnnee: string[] = [
    'unite',
    'codeStructure',
    'jeunes',
    'chefs',
    'quotaDir',
    'quotaEncadrants',
    'quotaQualifies',
    'quotaStagiaires',
    'quotaNonQualifies',
    'quotaPSC1',
    'actions',
  ];
  displayedColumnsQuotaParUnite: string[] = [
    'unite',
    'codeGroupe',
    'codeStructure',
    'branche',
    'jeunes',
    'chefs',
    'quotaDir',
    'quotaEncadrants',
    'quotaQualifies',
    'quotaStagiaires',
    'quotaNonQualifies',
    'quotaPSC1',
    'actions',
  ];
  displayedColumnsJS: string[] = [
    'codeAdherent',
    'nom',
    'prenom',
    'unite',
    'codeGroupe',
    'codeStructure',
    'fonction',
    'fonctionSecondaire',
    'bafapotentiel',
    'js_diplome',
    'js_diplomedetails',
    'js_qualite',
  ];
  displayedColumnsDroits: string[] = [
    'codeAdherent',
    'nom',
    'prenom',
    'nomStructure',
    'codeGroupe',
    'codeStructure',
    'fonction',
    'fonctionSecondaire',
    'droitImage',
    'droitInformations',
  ];
  displayedColumnsMarins: string[] = [
    'codeAdherent',
    'nom',
    'prenom',
    'nomStructure',
    'codeGroupe',
    'codeStructure',
    'fonction',
    'fonctionSecondaire',
    'pe',
    'cq',
    'cf',
  ];

  expandedSyntheseParUnite: boolean[] = [];
  allExpandedSyntheseParUnite = false;
  expandedQuotaAnnee: boolean[] = [];
  allExpandedQuotaAnnee = false;
  expandedQuotaParUnite: boolean[] = [];
  allExpandedQuotaParUnite = false;
  expandedFqd: boolean[] = [];
  allExpandedFqd = false;

  displayChargerStructure = false;

  @ViewChild('pSort') pSort?: MatSort;
  @ViewChild('fqdSort') fqdSort?: MatSort;
  @ViewChild('diversSort') diversSort?: MatSort;
  @ViewChild('marinsSort') marinsSort?: MatSort;
  @ViewChild('farfadetsSort') farfadetsSort?: MatSort;
  @ViewChild('compasSort') compasSort?: MatSort;
  @ViewChild('alertesSort') alertesSort?: MatSort;
  @ViewChild('droitsSort') droitsSort?: MatSort;

  constructor(
    @Inject(SESSION_STORAGE) protected sessionStorageService: StorageService,
    private router: Router,
    private route: ActivatedRoute,
    protected service: AnalyseService,
    private messageService: MessageService,
    private ngxService: NgxUiLoaderService
  ) {
    super(sessionStorageService, service);
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id === null) {
      this.loadAll();
    } else {
      this.goSingle(id);
    }
  }

  toggleFqd(i: number): void {
    this.expandedFqd[i] = !this.expandedFqd[i];
  }

  toggleAllFqd(stateToggle: boolean): void {
    this.allExpandedFqd = stateToggle;
    this.expandedFqd = [];
    this.values.forEach(() => {
      this.expandedFqd.push(stateToggle);
    });
  }

  testFormation(formations: any[], nom: string, formateur: boolean): boolean {
    let result = false;
    for (const formation of formations) {
      if (formation.formateur === formateur && nom === formation.type) {
        result = true;
        break;
      }
    }
    return result;
  }

  toggleSyntheseParUnite(i: number): void {
    this.expandedSyntheseParUnite[i] = !this.expandedSyntheseParUnite[i];
  }

  toggleAllSyntheseParUnite(stateToggle: boolean): void {
    this.allExpandedSyntheseParUnite = stateToggle;
    this.expandedSyntheseParUnite = [];
    if (stateToggle) {
      this.valuesSyntheseParUnite.forEach(() => {
        this.expandedSyntheseParUnite.push(true);
      });
    }
  }

  toggleQuotaAnnee(i: number): void {
    this.expandedQuotaAnnee[i] = !this.expandedQuotaAnnee[i];
  }

  toggleAllQuotaAnnee(stateToggle: boolean): void {
    this.allExpandedQuotaAnnee = stateToggle;
    this.expandedQuotaAnnee = [];
    if (stateToggle) {
      this.valuesQuotaAnnee.forEach(() => {
        this.expandedQuotaAnnee.push(true);
      });
    }
  }

  toggleQuotaParUnite(i: number): void {
    this.expandedQuotaParUnite[i] = !this.expandedQuotaParUnite[i];
  }

  toggleAllQuotaUnite(stateToggle: boolean): void {
    this.allExpandedQuotaParUnite = stateToggle;
    this.expandedQuotaParUnite = [];
    if (stateToggle) {
      this.valuesQuotaParUnite.forEach(() => {
        this.expandedQuotaParUnite.push(true);
      });
    }
  }

  goSingle(id: string): void {
    this.selectedStructures = [];
    this.menuReady = false;
    this.service.getStructureHie(id).subscribe(res => {
      if (res !== null) {
        this.selectedStructures.push(res);
      }
      this.menuReady = true;
      this.go(true);
    });
  }

  clear(): void {
    this.structureChargee = undefined;
    this.setValues([], []);
    super.clear();
    this.loadAll();
  }

  go(query: boolean): void {
    if (query === true) {
      this.ngxService.start();

      this.selectionStructuresChanged();

      const s: any[] = [];
      this.selectedStructures.forEach(s2 => {
        s.push(s2.data);
      });
      this.service.getResponsables(s).subscribe(
        res => {
          this.adherentsFull = res !== null ? res.adherents : [];
          this.alertesFull = res !== null ? res.alertes : [];
          this.valuesSyntheseParUnite = res !== null ? res.unites : [];
          this.valuesQuotaParUnite = res !== null ? res.quotaunite : [];
          this.valuesQuotaAnnee = res !== null ? res.quotaannee : [];
          this.setValues(this.adherentsFull, this.alertesFull);
          this.ngxService.stop();
        },
        err => {
          this.messageService.add({ key: 'tc', severity: 'warn', summary: 'Warn', detail: err.error.detail });
          this.ngxService.stop();
        },
        () => {
          this.ngxService.stop();
        }
      );
    }
    else {
      this.setValues(this.adherentsFull, this.alertesFull);
    }
  }

  isEnf(): boolean {
    const structureFonctions = this.sessionStorageService.get(ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS);
    if (structureFonctions.echelonStructure === 'National') {
      return true;
    }
    return false;
  }

  chargerStructure(): void {
    this.displayChargerStructure = true;
  }

  goChargerStructure(): void {
    this.displayChargerStructure = false;
    this.ngxService.start();
    super.loadAll(false, this.structureChargee);
    this.messageService.add({ key: 'tc', severity: 'info', summary: 'Info', detail: 'Structure chargée' });
    this.ngxService.stop();
  }

  protected loadAll(): void {
    super.loadAll(false, undefined);
  }

  private setValues(ads: any[], alertes: any[]): void {

    this.values = [];
    for (const ad of ads) {
      if (ad.status !== 'ADHERENT') {
        if (this.afficheNonAdherents === true) {
          this.values.push(ad);
        }
      }
      else {
        this.values.push(ad);
      }
    }
    this.valuesCompas = [];
    this.valuesFarfadets = [];
    this.expandedFqd = [];
    for (const element of this.values) {
      this.expandedFqd.push(false);
    }
    this.dataSource = new MatTableDataSource(this.values);
    this.dataSourceFqd = new MatTableDataSource(this.values);
    this.dataSourceDivers = new MatTableDataSource(this.values);
    this.dataSourceMarins = new MatTableDataSource(this.values);
    this.dataSourceDroits = new MatTableDataSource(this.values);
    if (
      this.pSort !== undefined &&
      this.fqdSort !== undefined &&
      this.diversSort !== undefined &&
      this.marinsSort !== undefined &&
      this.droitsSort !== undefined
    ) {
      this.dataSource.sort = this.pSort;
      this.dataSourceFqd.sort = this.fqdSort;
      this.dataSourceDivers.sort = this.diversSort;
      this.dataSourceMarins.sort = this.marinsSort;
      this.dataSourceDroits.sort = this.droitsSort;
    }
    for (const element of this.values) {
      if (element.compa === true) {
        this.valuesCompas.push(element);
      }
      if (element.farfadet === true) {
        this.valuesFarfadets.push(element);
      }
    }
    this.dataSourceCompas = new MatTableDataSource(this.valuesCompas);
    if (this.compasSort !== undefined) {
      this.dataSourceCompas.sort = this.compasSort;
    }
    this.dataSourceFarfadets = new MatTableDataSource(this.valuesFarfadets);
    if (this.farfadetsSort !== undefined) {
      this.dataSourceFarfadets.sort = this.farfadetsSort;
    }
    this.valuesAlertes = [];   
    for (const alerte of alertes) {
      if (alerte.adherent.status !== 'ADHERENT') {
        if (this.afficheNonAdherents === true) {
          this.valuesAlertes.push(alerte);
        }
      }
      else {
        this.valuesAlertes.push(alerte);
      }
    }
    this.dataSourceAlertes = new MatTableDataSource(this.valuesAlertes);
    if (this.alertesSort !== undefined) {
      this.dataSourceAlertes.sort = this.alertesSort;
    }
    this.expandedSyntheseParUnite = [];
    this.valuesSyntheseParUnite.forEach(() => {
      this.expandedSyntheseParUnite.push(false);
    });
    this.dataSourceSyntheseParUnite = new MatTableDataSource(this.valuesSyntheseParUnite);

    this.dataSourceQuotaParUnite = new MatTableDataSource(this.valuesQuotaParUnite);
    this.expandedQuotaParUnite = [];
    this.valuesQuotaParUnite.forEach(() => {
      this.expandedQuotaParUnite.push(false);
    });

    this.dataSourceQuotaAnnee = new MatTableDataSource(this.valuesQuotaAnnee);
    this.expandedQuotaAnnee = [];
    this.valuesQuotaAnnee.forEach(() => {
      this.expandedQuotaAnnee.push(false);
    });


  }
}
