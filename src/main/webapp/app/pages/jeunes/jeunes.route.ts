import { Routes } from '@angular/router';

import { JeunesComponent } from './jeunes.component';

const jeunesRoutes: Routes = [
  {
  path: '',
  component: JeunesComponent,
  title: 'pages.jeunes'
},
{
  path: ':id',
  component: JeunesComponent,
  title: 'pages.jeunes'
}
]

export default jeunesRoutes;