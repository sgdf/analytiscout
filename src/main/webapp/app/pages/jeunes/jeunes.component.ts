import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MessageService } from 'primeng/api';

import SharedModule from 'app/shared/shared.module';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { NgxUiLoaderModule, NgxUiLoaderService } from 'ngx-ui-loader';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';

import { AnalyseComponent } from '../analyse.component';
import { AnalyseService } from '../../analyse.service';
import ToFixedPipe from 'app/shared/tofixed.pipe';
import { FormsModule } from '@angular/forms';

@Component({
  standalone: true,
  selector: 'jhi-jeunes',
  templateUrl: './jeunes.component.html',
  styleUrls: ['./jeunes.component.scss', '../analyse.component.scss'],
  providers: [MessageService],
  imports: [
    SharedModule,
    NgxUiLoaderModule,
    ToFixedPipe,
    FormsModule
  ],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class JeunesComponent extends AnalyseComponent implements OnInit {
  values: any = [];
  valuesAlertes: any = [];
  dataSource: MatTableDataSource<any> = new MatTableDataSource(this.values);
  dataSourceDivers: MatTableDataSource<any> = new MatTableDataSource(this.values);
  dataSourceMarins: MatTableDataSource<any> = new MatTableDataSource(this.values);
  dataSourceAlertes: MatTableDataSource<any> = new MatTableDataSource(this.valuesAlertes);
  dataSourceDroits: MatTableDataSource<any> = new MatTableDataSource(this.values);

  displayedColumns: string[] = [
    'status',
    'codeAdherent',
    'nom',
    'prenom',
    'nomStructure',
    'codeGroupe',
    'codeStructure',
    'branche',
    'brancheAnneeProchaine',
    'age',
    'ageCamp',
    'ageAnneeOk',
    'ageCampOk',
  ];
  displayedColumnsAlertes: string[] = [
    'codeAdherent',
    'nom',
    'prenom',
    'unite',
    'codeGroupe',
    'codeStructure',
    'branche',
    'severite',
    'type',
    'alerte',
  ];
  displayedColumnsDroits: string[] = [
    'codeAdherent',
    'nom',
    'prenom',
    'unite',
    'codeGroupe',
    'codeStructure',
    'branche',
    'droitImage',
    'droitInformations',
  ];
  displayedColumnsMarins: string[] = [
    'codeAdherent',
    'nom',
    'prenom',
    'unite',
    'codeGroupe',
    'codeStructure',
    'fonction',
    'fonctionSecondaire',
    'pe',
    'cq',
    'cf',
  ];
  displayedColumnsDiplomesDivers: string[] = ['codeAdherent', 'nom', 'prenom', 'fonction', 'diplome_psc1'];

  @ViewChild('pSort') pSort?: MatSort;
  @ViewChild('marinsSort') marinsSort?: MatSort;
  @ViewChild('alertesSort') alertesSort?: MatSort;
  @ViewChild('diversSort') diversSort?: MatSort;
  @ViewChild('droitsSort') droitsSort?: MatSort;

  constructor(
    @Inject(SESSION_STORAGE) protected sessionStorageService: StorageService,
    private router: Router,
    private route: ActivatedRoute,
    protected service: AnalyseService,
    private messageService: MessageService,
    private ngxService: NgxUiLoaderService
  ) {
    super(sessionStorageService, service);
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id === null) {
      this.loadAll();
    } else {
      this.goSingle(id);
    }
  }

  goSingle(id: string): void {
    this.selectedStructures = [];
    this.menuReady = false;
    this.service.getStructureHie(id).subscribe(res => {
      if (res !== null) {
        this.selectedStructures.push(res);
      }
      this.menuReady = true;
      this.go(true);
    });
  }

  clear(): void {
    this.setValues([], []);
    super.clear();
  }

  go(query: boolean): void {
    if (query === true) {
      this.ngxService.start();

      this.selectionStructuresChanged();

      const s: any[] = [];
      for (const s2 of this.selectedStructures) {
        s.push(s2.data);
      };

      this.service.getJeunes(s).subscribe(
        res => {
          this.adherentsFull = res !== null ? res.adherents : [];
          this.alertesFull = res !== null ? res.alertes : [];
          this.setValues(this.adherentsFull, this.alertesFull);

          this.ngxService.stop();
        },
        err => {
          this.messageService.add({ key: 'tc', severity: 'warn', summary: 'Warn', detail: err.error.detail });
          this.ngxService.stop();
        },
        () => {
          this.ngxService.stop();
        }
      );
    }
    else {
      this.setValues(this.adherentsFull, this.alertesFull);
    }
  }

  loadAll(): void {
    super.loadAll(true, undefined);
  }

  private setValues(ads: any[], alertes: any[]): void {
    this.values = [];
    for (const ad of ads) {
      if (ad.status !== 'ADHERENT') {
        if (this.afficheNonAdherents === true) {
          this.values.push(ad);
        }
      }
      else {
        this.values.push(ad);
      }
    }
    this.dataSource = new MatTableDataSource(this.values);
    this.dataSourceDivers = new MatTableDataSource(this.values);
    this.dataSourceMarins = new MatTableDataSource(this.values);
    this.dataSourceDroits = new MatTableDataSource(this.values);
    this.dataSource = new MatTableDataSource(this.values);
    if (this.pSort !== undefined && this.diversSort !== undefined && this.marinsSort !== undefined && this.droitsSort !== undefined) {
      this.dataSource.sort = this.pSort;
      this.dataSourceDivers.sort = this.diversSort;
      this.dataSourceMarins.sort = this.marinsSort;
      this.dataSourceDroits.sort = this.droitsSort;
    }
    this.valuesAlertes = [];   
    for (const alerte of alertes) {
      if (alerte.adherent.status !== 'ADHERENT') {
        if (this.afficheNonAdherents === true) {
          this.valuesAlertes.push(alerte);
        }
      }
      else {
        this.valuesAlertes.push(alerte);
      }
    }
    this.dataSourceAlertes = new MatTableDataSource(this.valuesAlertes);
    if (this.alertesSort !== undefined) {
      this.dataSourceAlertes.sort = this.alertesSort;
    }
  }
}
