import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { JeunesComponent } from './jeunes.component';
import { jeunesRoutes } from './jeunes.route';
import { NgxUiLoaderModule } from "ngx-ui-loader";
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';

@NgModule({
  imports: [NgbModule, SharedModule, RouterModule.forChild(jeunesRoutes),
    NgxUiLoaderModule],
  declarations: [JeunesComponent],
  exports: [JeunesComponent],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' }
    }
  ],
})
export class JeunesModule { }

