import { Routes } from '@angular/router';
const routes: Routes = [
    {
        path: 'responsables',
        data: { pageTitle: 'responsables.home.title' },
        loadChildren: () => import('./responsables/responsables.route'),
    },
    {
        path: 'jeunes',
        data: { pageTitle: 'jeunes.home.title' },
        loadChildren: () => import('./jeunes/jeunes.route'),
    },
]

export default routes;