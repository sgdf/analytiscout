import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'responsables',
        loadChildren: () => import('./responsables/responsables.module').then(m => m.ResponsablesModule),
        data: {
          pageTitle: 'responsables.home.title'
        }
      },
      {
        path: 'jeunes',
        loadChildren: () => import('./jeunes/jeunes.module').then(m => m.JeunesModule),
        data: {
          pageTitle: 'jeunes.home.title'
        }
      }
    ])
  ]
})
export class PagesRoutingModule { }
