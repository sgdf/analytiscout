
import { Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { AnalyseService } from '../analyse.service';
import { ANALYTISCOUT_STORAGE_SELECTED_STRUCTURES, ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS, ANALYTISCOUT_STORAGE_STRUCTURE_CHARGEES } from '../app.constants'

export class AnalyseComponent {

    structures: any[] = [];
    selectedStructures: any[] = [];
    selectedStructuresCodes: any[] = [];
    menuReady = false;
    structureChargee : any;
    afficheNonAdherents = false;
    adherentsFull: any[] = [];
    alertesFull: any[] = [];

    constructor(@Inject(SESSION_STORAGE) protected sessionStorageService: StorageService, protected service: AnalyseService) {
    }

    selectedStructuresContainsBranche(branche: string | undefined): boolean {
        if (branche === undefined) {
            return (this.selectedStructures.length === 0);
        }
        for (const structure of this.selectedStructures) {
            if (structure.branche === branche) {
                return true;
            }
        }
        return false;
    }

    selectionStructuresChanged(): void {
        this.selectedStructuresCodes = [];
        for (const structure of this.selectedStructures) {
            if (structure.codeStructure !== 0) {
                this.selectedStructuresCodes.push(structure.codeStructure);
            }
            this.selectedStructuresCodes.push(structure.codeStructure);
        }
        this.sessionStorageService.set(ANALYTISCOUT_STORAGE_SELECTED_STRUCTURES, this.selectedStructuresCodes);
    }

    showQuotas(): boolean {
        const structuresChargees = this.sessionStorageService.get(ANALYTISCOUT_STORAGE_STRUCTURE_CHARGEES);
        if (structuresChargees !== undefined) {
            let tousNonNational = true;
            for (const structure of structuresChargees) {
                if (structure.echelonStructure === "National") {
                    tousNonNational = false;
                }
            }
            return tousNonNational;
        }
        const structureFonctions = this.sessionStorageService.get(ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS);
        if (structureFonctions.echelonStructure === "National") {
            return false;
        }
        return true;
    }

    testDiplome(diplomes : any[], nom : string) : boolean {
      let result  = false;
      for (const diplome of diplomes) {
        if (nom === diplome.type) {
          result = true;
          break;
        }
      }
      return result;
    }

    clearStructureChargee(jeunes: boolean) : void {
        this.sessionStorageService.remove(ANALYTISCOUT_STORAGE_STRUCTURE_CHARGEES);
        this.structureChargee = undefined;
        this.clear();
        this.loadAll(jeunes, undefined);
    }

    clear() : void {
        this.selectedStructures = [];
        this.sessionStorageService.remove(ANALYTISCOUT_STORAGE_SELECTED_STRUCTURES);
    }

    protected loadAll(jeunes: boolean, codeStructure : string | undefined): void {
        this.selectedStructures = [];
        this.menuReady = false;
        if (codeStructure === undefined) {
            const structureChargees = this.sessionStorageService.get(ANALYTISCOUT_STORAGE_STRUCTURE_CHARGEES);
            if (structureChargees !== undefined) {
                this.structures = structureChargees;
                this.menuReady = true;
                const v2 = this.sessionStorageService.get(ANALYTISCOUT_STORAGE_SELECTED_STRUCTURES);
                if (v2 !== undefined) {
                    this.selectedStructuresCodes = v2;
                    const codes: Set<any> = new Set<any>();
                    this.selectedStructuresCodes.forEach(v => codes.add(v));
                    this.selectMenu();
                }
            }
            else {
                const structureFonctions = this.sessionStorageService.get(ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS);
                this.service.getStructuresHie(jeunes, structureFonctions).subscribe(res => {
                    this.structures = res;
                    this.menuReady = true;
                    const v2 = this.sessionStorageService.get(ANALYTISCOUT_STORAGE_SELECTED_STRUCTURES);
                    this.selectMenu();
                });
            }
        }
        else {
            this.service.getStructureHieFull(codeStructure).subscribe(res => {
                this.structures = res;
                this.menuReady = true;
                this.sessionStorageService.set(ANALYTISCOUT_STORAGE_STRUCTURE_CHARGEES, this.structures);
                this.sessionStorageService.remove(ANALYTISCOUT_STORAGE_SELECTED_STRUCTURES);
            });
        }
    }

    private selectMenu(): void {
        const v2 = this.sessionStorageService.get(ANALYTISCOUT_STORAGE_SELECTED_STRUCTURES);
        if (v2 !== undefined) {
            this.selectedStructuresCodes = v2;
            const codes: Set<any> = new Set<any>();
            this.selectedStructuresCodes.forEach(v => codes.add(v));
            this.searchSelected(codes, this.structures);
        }
    }

    private searchSelected(codes: Set<any>, structures: any[]): void {
        for (const s of structures) {
            if (s.selectable === true && codes.has(s.codeStructure)) {
                this.selectedStructures.push(s);
            };
            this.searchSelected(codes, s.children);
        }
    }
}