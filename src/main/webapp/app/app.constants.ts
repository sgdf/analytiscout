// These constants are injected via webpack DefinePlugin variables.
// You can add more variables in webpack.common.js or in profile specific webpack.<dev|prod>.js files.
// If you change the values in the webpack config files, you need to re run webpack to update the application

declare const __DEBUG_INFO_ENABLED__: boolean;
declare const __VERSION__: string;
declare const __SERVER_API_URL__: string;

export const VERSION = __VERSION__;
export const DEBUG_INFO_ENABLED = __DEBUG_INFO_ENABLED__;
export const SERVER_API_URL = __SERVER_API_URL__;

export const ANALYTISCOUT_STORAGE_SELECTED_STRUCTURES = "selectedStructures";
export const ANALYTISCOUT_STORAGE_STRUCTURE_FONCTIONS = "structureFonctions";
export const ANALYTISCOUT_STORAGE_STRUCTURE_CHARGEES = "structuresChargees";
export const ANALYTISCOUT_STORAGE_SELECTED_STRUCTURES_FONCTIONS = "selectedStructuresFonctions";
