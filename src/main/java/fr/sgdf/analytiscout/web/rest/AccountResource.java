package fr.sgdf.analytiscout.web.rest;

import java.security.Principal;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.WebApplicationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonCreator;

import fr.sgdf.analytiscout.dev.service.AdherentService;
import fr.sgdf.analytiscout.dev.service.AdminUserService;
import fr.sgdf.analytiscout.dev.service.IntranetCache;
import fr.sgdf.analytiscout.dev.service.StructureService;
import fr.sgdf.analytiscout.dev.service.UserVMExtended;
import fr.sgdf.analytiscout.security.SecurityUtils;
import fr.sgdf.analytiscout.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import redis.clients.jedis.Jedis;

@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private static class AccountResourceException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        private AccountResourceException(String message) {
            super(message);
        }

        private AccountResourceException() {
            super("AccountResourceException");
        }
    }

	final private IntranetCache cacheUtilisateurs;
    private StructureService structureService;
    private AdherentService adherentService;
    private AdminUserService adminUserService;

    public AccountResource(IntranetCache cacheUtilisateurs, StructureService structureService, AdherentService adherentService, UserService userService, AdminUserService adminUserService) {
        this.structureService = structureService;
    	this.adherentService = adherentService;
    	this.cacheUtilisateurs = cacheUtilisateurs;
    	this.adminUserService = adminUserService;
    }
    /**
     * {@code GET  /account} : get the current user.
     *
     * @param principal the current user; resolves to {@code null} if not authenticated.
     * @return the current user.
     * @throws AccountResourceException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/account")
    @SuppressWarnings("unchecked")
    public UserVMExtended getAccount(Principal principal) {
        String login = SecurityUtils.getCurrentUserLogin().orElseThrow(AccountResourceException::new);
        UserVMExtended u = adminUserService.getUserFromAuthentication((AbstractAuthenticationToken)principal);
        try (Jedis cache = cacheUtilisateurs.get()) {
        	u.structuresFonctions = structureService.structuresFonctions(cache, login);
            u.adherent = adherentService.getCurrentAdherentFull(cache, login);
        } catch (Exception e) {
        	throw new WebApplicationException(e);
		}
        return u;
    }

    /**
     * {@code GET  /authenticate} : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request.
     * @return the login if the user is authenticated.
     */
    @GetMapping("/authenticate")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    private static class UserVM {

        private String login;
        private Set<String> authorities;

        @JsonCreator
        UserVM(String login, Set<String> authorities) {
            this.login = login;
            this.authorities = authorities;
        }

        public boolean isActivated() {
            return true;
        }

        public Set<String> getAuthorities() {
            return authorities;
        }

        public String getLogin() {
            return login;
        }
    }

    private UserVM getUserFromAuthentication(AbstractAuthenticationToken authToken) {
        if (!(authToken instanceof OAuth2AuthenticationToken) && !(authToken instanceof JwtAuthenticationToken)) {
            throw new IllegalArgumentException("AuthenticationToken is not OAuth2 or JWT!");
        }

        return new UserVM(
            authToken.getName(),
            authToken.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet())
        );
    }
}
