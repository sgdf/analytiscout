package fr.sgdf.analytiscout.dev;

import java.util.ArrayList;
import java.util.List;

import fr.sgdf.intranetapi.ApiCall;
import fr.sgdf.intranetapi.exceptions.ApiSessionException;
import fr.sgdf.intranetapi.modeles.structure.MembreAssocie;

public class ApiUtils {
	public static List<Long> responsables(ApiCall api, List<Integer> structures) throws ApiSessionException {
		List<Long> ids = new ArrayList<Long>();
		for (Integer structure : structures) {
			List<fr.sgdf.intranetapi.modeles.structure.Responsable> sResponsables = api.responsables(structure);
			sResponsables.forEach(responsable -> ids.add(responsable.codeAdherent));
		}
		return ids;
	}
	
	public static List<Long> convertResponsables(List<fr.sgdf.intranetapi.modeles.structure.Responsable> sResponsables) {
		List<Long> ids = new ArrayList<Long>();
		sResponsables.forEach(responsable -> ids.add(responsable.codeAdherent));
		return ids;
	}
	
	public static List<Long> membresAssocies(ApiCall api, List<Integer> structures) throws ApiSessionException {
		List<Long> ids = new ArrayList<Long>();
		for (Integer structure : structures) {
			List<MembreAssocie> sMembresAssocies = api.membresAssocies(structure);
			sMembresAssocies.forEach(membresAssocie -> ids.add(membresAssocie.codeAdherent));
		}
		return ids;
	}
	
	public static List<Long> jeunes(ApiCall api, List<Integer> structures) throws ApiSessionException {
		List<Long> ids = new ArrayList<Long>();
		
		for (Integer structure : structures) {
			List<fr.sgdf.intranetapi.modeles.structure.Jeune> sJeunes = api.jeunes(structure);
			sJeunes.forEach(jeune -> ids.add(jeune.codeAdherent));
		}
		return ids;
	}
	
	public static List<Long> convertJeunes(List<fr.sgdf.intranetapi.modeles.structure.Jeune> sJeunes) {
		List<Long> ids = new ArrayList<Long>();
		sJeunes.forEach(responsable -> ids.add(responsable.codeAdherent));
		return ids;
	}
}
