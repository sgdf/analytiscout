package fr.sgdf.analytiscout.dev;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import fr.sgdf.intranetapi.modeles.adherent.Branche;
import fr.sgdf.intranetapi.utils.StructureUtils;

public class Utils {
	
	private static final Map<String, String> iconesFaDiplomes= new TreeMap<String, String>();
	private static final Map<String, String> iconesSgdfDiplomes = new TreeMap<String, String>();
	private static final Map<String, String> iconesSgdfDiplomesClasse = new TreeMap<String, String>();

	private static final Map<String, String> iconesFaFormations = new TreeMap<String, String>();
	private static final Map<String, String> iconesSgdfFormations = new TreeMap<String, String>();
	
	private static Set<String> typesFormationDivers = new TreeSet<String>();
	
	static {
		iconesFaDiplomes.put("PSC1 Prevention et Secours Civiques de niveau 1", "suitcase-medical");
		iconesFaDiplomes.put("AFPS", "briefcase-medical");
		iconesFaDiplomes.put("Sauveteur Secouriste du Travail", "briefcase-medical");
		iconesFaDiplomes.put("PSE1 Premiers Secours en Equipe de niveau 1", "briefcase-medical");

		iconesFaDiplomes.put("Médaille Argent de la JS", "medal");
		iconesFaDiplomes.put("Médaille Bronze de la JS", "medal");
		iconesFaDiplomes.put("Médaille Or de la JS", "medal");
		
		iconesSgdfDiplomes.put("Nœud de tisserand", "sgdf-picto-signesImpessa-NoeudTisserand");
		iconesSgdfDiplomes.put("Patron d'embarcation", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("Chef de quart", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("Chef de Flottille", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("BAFA Qualification Voile", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("Surveillant de Baignade", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("Brevet d'Etat (activités marines)", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("Carte Mer", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("Brevet Professionnel (Skipper, marine marchande, .", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("Directeur nautique", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("Permis Côtier", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("Permis fluvial", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("Permis Hauturier", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("Certificat Radio Restreint CRR", "sgdf-picto-signesCommuns-Ancre");
		iconesSgdfDiplomes.put("Croix du Père Sevin", "sgdf-picto-signesCommuns-CroixScoute");
		iconesSgdfDiplomes.put("2 Buchettes", "sgdf-picto-signesFormation-Buchettes");
		iconesSgdfDiplomes.put("3 Buchettes", "sgdf-picto-signesFormation-Buchettes");
		iconesSgdfDiplomes.put("4 Buchettes", "sgdf-picto-signesFormation-Buchettes");
	}
	
	static {
		typesFormationDivers.add("E-LEARNING À L’ABRI DE LA MALTRAITANCE");
		typesFormationDivers.add("E-LEARNING SGDF BIENVENUE");
	}
	
	public static boolean isFormationDivers(String type) {
		return typesFormationDivers.contains(type);
	}
	
	public static String iconeFaFormation(String type) {
		return iconesFaFormations.get(type);
	}
	
	public static String iconeSgdfFormation(String type) {
		return iconesSgdfFormations.get(type);
	}
	
	public static String iconeFaDiplome(String type) {
		return iconesFaDiplomes.get(type);
	}
	
	public static String iconeSgdfDiplome(String type) {
		return iconesSgdfDiplomes.get(type);
	}
	
	public static boolean iconeSgdfDiplomeClasseLarge(String type) {
		return iconesSgdfDiplomesClasse.containsKey(type);
	}

	public static String calculClasseFonction(int code)
	{
		String sCode = "" + code;
		if (sCode.startsWith("19") || sCode.startsWith("29")) {
			return "audace";
		}
		if (sCode.startsWith("24") || sCode.startsWith("14")) {
			return "compa";
		}
		if (sCode.startsWith("8") || sCode.startsWith("9") || sCode.startsWith("6") || sCode.startsWith("5") || sCode.startsWith("4")
				|| sCode.startsWith("3")) {
			return "violet";
		}
		if (sCode.startsWith("27")) {
			return "farfadet";
		}
		if (sCode.startsWith("23") || sCode.startsWith("13")) {
			return "piok";
		}
		if (sCode.startsWith("22") || sCode.startsWith("12")) {
			return "sg";
		}
		if (sCode.startsWith("21") || sCode.startsWith("11")) {
			return "lj";
		}

		return null;
	}
	
	public static String getCodebranche(int codeStructure)
	{
		if (StructureUtils.isNationale(codeStructure)) {
			return "0";
		}
		String cs = codeStructure+"";
		String codeBranche = cs.substring(cs.length()-2, cs.length()-1);
		return codeBranche;
	}
	
	public static int extraitCodeFonction(String codeFonction)
	{
		char lcode = codeFonction.charAt(codeFonction.length()-1);
		if (lcode > 'A')
			codeFonction = codeFonction.substring(0, 3);
		return Integer.valueOf(codeFonction);
	}
	
	public static boolean getCodeterritoire(Integer codeStructureAdherent, int codeStructure)
	{
		if (StructureUtils.isNationale(codeStructureAdherent) == true) {
			return (codeStructure == codeStructureAdherent);
		}
		if (StructureUtils.isTerritoire(codeStructureAdherent) == true) {
			int codeTerritoireAdherent = StructureUtils.getCodeTerritoire(codeStructureAdherent);
			int codeTerritoire = StructureUtils.getCodeTerritoire(codeStructure);
			return (codeTerritoireAdherent == codeTerritoire);
		}
		if (StructureUtils.isGroupe(codeStructureAdherent) == true) {
			int codeGroupeAdherent = StructureUtils.getCodeGroupe(codeStructureAdherent);
			int codeGroupe = StructureUtils.getCodeGroupe(codeStructure);
			return (codeGroupeAdherent == codeGroupe);
		}
		if (StructureUtils.isUnite(codeStructureAdherent) == true) {
			return (codeStructureAdherent == codeStructure);
		}
		return false;
	}

	public static String calculAbreviationBranche(Branche branche) {
		if (branche == Branche.FARFADET)
		{
			return "F";
		}
		if (branche == Branche.LOUVETEAU_JEANNETTE)
		{
			return "LJ";
		}
		if (branche == Branche.SCOUT_GUIDE)
		{
			return "SG";
		}
		if (branche == Branche.PIONNIER_CARAVELLE)
		{
			return "PC";
		}
		if (branche == Branche.COMPAGNON)
		{
			return "C";
		}
		if (branche == Branche.AUDACE)
		{
			return "A";
		}
		return "R";
	}
	
	public static String calculClasseBranche(Branche branche)
	{
		if (branche == Branche.FARFADET)
		{
			return "farfadet";
		}
		if (branche == Branche.LOUVETEAU_JEANNETTE)
		{
			return "lj";
		}
		if (branche == Branche.SCOUT_GUIDE)
		{
			return "sg";
		}
		if (branche == Branche.PIONNIER_CARAVELLE)
		{
			return "piok";
		}
		if (branche == Branche.COMPAGNON)
		{
			return "compa";
		}
		if (branche == Branche.AUDACE)
		{
			return "audace";
		}
		return "violet";
	}
	
	public static Branche calculBrancheJeune(int codeStructure, Date dateDeNaissance)
	{
		Branche brancheCalcule = StructureUtils.getBrancheFromCode(codeStructure);
		Date debutFindDec = Params.getDateLimiteJeune();
		double diffFindDec = ((debutFindDec.getTime() - dateDeNaissance.getTime())/1000);
		diffFindDec = diffFindDec/(3600*365.25*24);
		if (diffFindDec < 8)
		{
			if (brancheCalcule != Branche.FARFADET)
				return brancheCalcule;
			return Branche.FARFADET;
		}
		if (diffFindDec < 11)
		{
			if (brancheCalcule != Branche.LOUVETEAU_JEANNETTE)
				return brancheCalcule;
			return Branche.LOUVETEAU_JEANNETTE;
		}
		if (diffFindDec < 14)
		{
			if (brancheCalcule != Branche.SCOUT_GUIDE)
				return brancheCalcule;
			return Branche.SCOUT_GUIDE;
		}
		if (diffFindDec < 17)
		{
			if (brancheCalcule != Branche.PIONNIER_CARAVELLE)
				return brancheCalcule;
			return Branche.PIONNIER_CARAVELLE;
		}
		if (diffFindDec < 22)
		{
			if (brancheCalcule != Branche.COMPAGNON)
				return brancheCalcule;
			return Branche.COMPAGNON;
		}
		return brancheCalcule;
	}

	public static Branche calculBrancheAnneeProchaineJeune(Integer codeStructure, Date dateDeNaissance) {
		
		Branche brancheCalcule = StructureUtils.getBrancheFromCode(codeStructure);
		if (brancheCalcule == Branche.AUDACE) return Branche.AUDACE;
		
		Date debutFindDec = Params.getDateLimiteJeune();
		double diffFindDec = ((debutFindDec.getTime() - dateDeNaissance.getTime())/1000);
		diffFindDec = diffFindDec/(3600*365.25*24);
		Date debutFindDecN1 = Params.getDateLimiteJeuneSuivant();
		double diffFindDecN1 = ((debutFindDecN1.getTime() - dateDeNaissance.getTime())/1000);
		diffFindDecN1 = diffFindDecN1/(3600*365.25*24);
				
		if (diffFindDecN1 < 8)
		{
			return Branche.FARFADET;
		}
		if (diffFindDecN1 < 11)
		{
			return Branche.LOUVETEAU_JEANNETTE;
		}
		if (diffFindDecN1 < 14)
		{
			return Branche.SCOUT_GUIDE;
		}
		if (diffFindDecN1 < 17)
		{
			return Branche.PIONNIER_CARAVELLE;
		}
		if (diffFindDecN1 < 22)
		{
			return Branche.COMPAGNON;
		}
		return Branche.ADULTE;
	}
}
