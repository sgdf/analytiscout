package fr.sgdf.analytiscout.dev.web;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.sgdf.analytiscout.config.ApplicationProperties;
import fr.sgdf.analytiscout.dev.modele.Responsable;
import fr.sgdf.analytiscout.dev.service.IntranetCache;
import fr.sgdf.intranetapi.exceptions.ApiException;
import fr.sgdf.intranetapi.modeles.adherent.Adherent;
import redis.clients.jedis.Jedis;

@RestController
@RequestMapping("/api/analytiscout")
public class ApiAdherents extends fr.sgdf.analytiscout.dev.UserExtraUtils {

	@Value("${jhipster.clientApp.name}")
    private String applicationName;
	
	private final Logger log = LoggerFactory.getLogger(ApiAdherents.class);
	
	final private IntranetCache cacheUtilisateurs;
    final private ApplicationProperties appProperties;

	public ApiAdherents(IntranetCache cacheUtilisateurs, ApplicationProperties appProperties) {
		super();
        this.cacheUtilisateurs = cacheUtilisateurs;
        this.appProperties = appProperties;
    }
	
	@GetMapping(value = "/adherent/{id}/full")
    public ResponseEntity<Responsable> getAdherentFull(@PathVariable Long id) throws IOException {
    	log.info("REST request to get adherent {}",id);
    	try (Jedis cache = cacheUtilisateurs.get()) {
			id = cacheUtilisateurs.translateAdherent(cache, id);
			Adherent adherent = cacheUtilisateurs.getAdherentFull(cache, id);
			
			Responsable responsable = new Responsable();
			responsable.transform(adherent);
			if (appProperties.getIntranet().isAnonymiser()) responsable.anonymiser();
			
			return ResponseEntity.ok().body(responsable);
		} catch (ApiException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Adhérent inconnu");
		}
    }
}
