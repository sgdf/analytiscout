package fr.sgdf.analytiscout.dev.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.sgdf.analytiscout.config.ApplicationProperties;
import fr.sgdf.analytiscout.dev.modele.Version;

@RestController
@RequestMapping("/api/analytiscout")
public class ApiResourceAdmin {
	@Value("${jhipster.clientApp.name}")
    private String applicationName;
	
    private final Logger log = LoggerFactory.getLogger(ApiResourceAdmin.class);
    
    final private ApplicationProperties appProperties;
	
    public ApiResourceAdmin(ApplicationProperties appProperties) {
        this.appProperties = appProperties;
    }
    
    @GetMapping("/infos/version")
    public Version getVersion() {
        log.debug("REST request to get version");
        return Version.parse(appProperties.getWork().isActive(), appProperties.getWork().getWorkMessage());
    }
}
