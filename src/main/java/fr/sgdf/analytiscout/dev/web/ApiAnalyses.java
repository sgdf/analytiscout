package fr.sgdf.analytiscout.dev.web;

import fr.sgdf.analytiscout.config.ApplicationProperties;
import fr.sgdf.analytiscout.dev.ApiUtils;
import fr.sgdf.analytiscout.dev.modele.Jeune;
import fr.sgdf.analytiscout.dev.modele.Jeunes;
import fr.sgdf.analytiscout.dev.modele.Responsable;
import fr.sgdf.analytiscout.dev.modele.Responsables;
import fr.sgdf.analytiscout.dev.modele.StructureHie;
import fr.sgdf.analytiscout.dev.modele.Structures;
import fr.sgdf.analytiscout.dev.modele.structureRequest;
import fr.sgdf.analytiscout.dev.service.IntranetApi;
import fr.sgdf.analytiscout.dev.service.IntranetCache;
import fr.sgdf.analytiscout.dev.service.StructureService;
import fr.sgdf.intranetapi.ApiCall;
import fr.sgdf.intranetapi.modeles.adherent.Adherent;
import fr.sgdf.intranetapi.modeles.adherent.Branche;
import fr.sgdf.intranetapi.modeles.adherent.Statut;
import fr.sgdf.intranetapi.modeles.adherent.diplome.Diplome;
import fr.sgdf.intranetapi.modeles.adherent.formation.Formation;
import fr.sgdf.intranetapi.modeles.adherent.qualification.Qualification;
import fr.sgdf.intranetapi.modeles.structure.Structure;
import fr.sgdf.intranetapi.utils.StructureUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ws.rs.WebApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;

@RestController
@RequestMapping("/api/analytiscout")
public class ApiAnalyses extends fr.sgdf.analytiscout.dev.UserExtraUtils {

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final Logger log = LoggerFactory.getLogger(ApiAnalyses.class);

    @Autowired
    private IntranetApi apiManager;

    private final IntranetCache cacheUtilisateurs;
    private final ApplicationProperties appProperties;
    private final StructureService structureService;

    public ApiAnalyses(StructureService structureService, IntranetCache cacheUtilisateurs, ApplicationProperties appProperties) {
        this.cacheUtilisateurs = cacheUtilisateurs;
        this.structureService = structureService;
        this.appProperties = appProperties;
    }

    @PostMapping(value = "/structures")
    public List<Structure> structures(@RequestBody structureRequest structureFonctions) {
        String login = getConnectedLogin();
        try (Jedis cache = cacheUtilisateurs.get()) {
            return structureService.structures(cache, login, structureFonctions);
        } catch (Exception e) {
            log.error("structures", e);
        }
        return null;
    }

    @GetMapping(value = "/structureHie/{id}")
    public StructureHie structuresHie(@PathVariable Integer id) {
        String login = getConnectedLogin();
        try (Jedis cache = cacheUtilisateurs.get()) {
            return structureService.structuresHie(cache, login, id);
        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
    }

    @GetMapping(value = "/structureHie/{id}/full")
    public List<StructureHie> structuresHieFull(@PathVariable Integer id) {
        String login = getConnectedLogin();
        try (Jedis cache = cacheUtilisateurs.get()) {
            return structureService.structuresHies(cache, login, id);
        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
    }

    @PostMapping(value = "/structuresHie")
    public List<StructureHie> structuresHie(@RequestBody structureRequest structureFonctions) {
        String login = getConnectedLogin();
        try (Jedis cache = cacheUtilisateurs.get()) {
            return structureService.structuresHie(cache, login, structureFonctions, null);
        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
    }

    @PostMapping(value = "/responsables")
    public Responsables responsables(@RequestBody Structures structures) {
        String login = getConnectedLogin();

        log.info("REST request to get responsables from {}", structures.structures.size());
        structures.check();

        Responsables rResponsables = new Responsables();

        try (Jedis cache = cacheUtilisateurs.get()) {
        	String idsSt = structures.structures.stream().map(s -> s.codeStructure.toString()).collect(Collectors.joining( "," ) );
            log.info("Responsables des structures {}", idsSt);

            List<Adherent> adultesApi = null;
            List<Long> idAdultes = new ArrayList<Long>();
            List<Long> idJeunes = null;
            try {
                ApiCall api = apiManager.createCall();
                List<Integer> idsResponsables = structures.structures.stream().filter(s -> isStructureMembresAssocies(s) == false).map(s -> s.codeStructure).collect(Collectors.toList());
                List<Long> idResponsables = ApiUtils.responsables(api, idsResponsables);
                
                List<Integer> idsMembresAssocies = structures.structures.stream().filter(s -> isStructureMembresAssocies(s) == true).map(s -> s.codeStructure).collect(Collectors.toList());
                List<Long> idMembresAssocies = ApiUtils.membresAssocies(api, idsMembresAssocies);
                
                idJeunes = ApiUtils.jeunes(api, idsResponsables);

                log.info("Responsables des structures ids {}", idResponsables.size());
                log.info("Membres associes des structures ids {}", idMembresAssocies.size());
                log.info("jeunes des structures ids {}", idJeunes.size());
                
                idAdultes.addAll(idResponsables);
                idAdultes.addAll(idMembresAssocies);
            } catch (Exception e) {
                log.error("responsables ids / jeunes ids", e);
            }

            adultesApi = cacheUtilisateurs.findAllFull(cache, login, idAdultes);
            List<Adherent> responsables = adultesApi
                .stream()
                .filter(a -> isAdherent(a, appProperties.getIntranet().isFiltrage()))
                .collect(Collectors.toList());

            List<Adherent> jAdherentsApi = cacheUtilisateurs.findAllFull(cache, login, idJeunes);
            List<Adherent> jAdherents = jAdherentsApi
                .stream()
                .filter(a -> isAdherent(a, appProperties.getIntranet().isFiltrage()))
                .collect(Collectors.toList());

            List<Adherent> responsablesCompas = new ArrayList<Adherent>();
            jAdherents.forEach(jAdherent -> {
                if (jAdherent.branche == Branche.COMPAGNON) responsablesCompas.add(jAdherent);
                if (
                    jAdherent.fonctionsSecondaires != null &&
                    jAdherent.fonctionsSecondaires.size() > 0 &&
                    (
                        jAdherent.fonctionsSecondaires.get(0).fonction.code.startsWith("" + 140) ||
                        jAdherent.fonctionsSecondaires.get(0).fonction.code.startsWith("" + 141)
                    )
                ) responsablesCompas.add(jAdherent);
            });
            rResponsables.transform(responsables);
            rResponsables.transform(responsablesCompas);

            Map<Long, List<Qualification>> qualificationsMap = cacheUtilisateurs.getQualificationsFull(cache, idAdultes);
            qualificationsMap.forEach((key, values) -> {
                Responsable r = rResponsables.getResponsable(key);
                if (r != null) values.forEach(q -> r.addQualification(q));
            });
            qualificationsMap = cacheUtilisateurs.getQualificationsFull(cache, idJeunes);
            qualificationsMap.forEach((key, values) -> {
                Responsable r = rResponsables.getResponsable(key);
                if (r != null) values.forEach(q -> r.addQualification(q));
            });

            Map<Long, List<Formation>> formationsMap = cacheUtilisateurs.getFormationsFull(cache, idAdultes);
            formationsMap.forEach((key, values) -> {
                Responsable r = rResponsables.getResponsable(key);
                if (r != null) values.forEach(q -> r.addFormation(q));
            });
            formationsMap = cacheUtilisateurs.getFormationsFull(cache, idJeunes);
            formationsMap.forEach((key, values) -> {
                Responsable r = rResponsables.getResponsable(key);
                if (r != null) values.forEach(q -> r.addFormation(q));
            });

            Map<Long, List<Diplome>> diplomesMap = cacheUtilisateurs.getDiplomesFull(cache, idAdultes);
            diplomesMap.forEach((key, values) -> {
                Responsable r = rResponsables.getResponsable(key);
                if (r != null) values.forEach(q -> r.addDiplome(q));
            });
            diplomesMap = cacheUtilisateurs.getDiplomesFull(cache, idJeunes);
            diplomesMap.forEach((key, values) -> {
                Responsable r = rResponsables.getResponsable(key);
                if (r != null) values.forEach(q -> r.addDiplome(q));
            });
            
            if (appProperties.getIntranet().isAnonymiser()) rResponsables.anonymiser();
            rResponsables.calcul(jAdherents);
        } catch (Exception e) {
            throw new WebApplicationException(e);
        }

        rResponsables.calculFinal();
        rResponsables.complete();

        rResponsables.trier();

        return rResponsables;
    }

    @PostMapping(value = "/jeunes")
    public Jeunes jeunes(@RequestBody Structures structures) {
        String login = getConnectedLogin();

        String idsSt = structures.structures.stream().map(s -> s.codeStructure.toString()).collect(Collectors.joining( "," ) );
        log.info("REST request to get jeunes from {}", idsSt);
        structures.check();

        Jeunes rJeunes = new Jeunes();

        try (Jedis cache = cacheUtilisateurs.get()) {
            log.info("Jeunes des structures {}", structures.structures.size());

            List<Long> idJeunes = null;
            try {
                ApiCall api = apiManager.createCall();
                List<Integer> ids = structures.structures.stream().map(s -> s.codeStructure).collect(Collectors.toList());
                idJeunes = ApiUtils.jeunes(api, ids);
                log.info("jeunes des structures ids {}", idJeunes.size());
            } catch (Exception e) {
                log.error("responsables", e);
            }

            List<Adherent> jeunesApi = cacheUtilisateurs.findAllFull(cache, login, idJeunes);
            List<Adherent> jeunes = jeunesApi
                .stream()
                .filter(a -> isAdherent(a, appProperties.getIntranet().isFiltrage()))
                .collect(Collectors.toList());
            rJeunes.transform(jeunes);

            Map<Long, List<Formation>> formationsMap = cacheUtilisateurs.getFormationsFull(cache, idJeunes);
            formationsMap.forEach((key, values) -> {
                Jeune r = rJeunes.getJeune(key);
                if (r != null) values.forEach(q -> r.addFormation(q));
            });

            Map<Long, List<Diplome>> diplomesMap = cacheUtilisateurs.getDiplomesFull(cache, idJeunes);
            diplomesMap.forEach((key, values) -> {
                Jeune r = rJeunes.getJeune(key);
                if (r != null) values.forEach(q -> r.addDiplome(q));
            });
        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
        rJeunes.calcul(true);
        rJeunes.complete();
        rJeunes.trier();
        return rJeunes;
    }

    boolean isAdherent(Adherent ad, boolean filtrage) {
        return !filtrage || (filtrage && (ad.statutE == Statut.ADHERENT || ad.statutE == Statut.PREINSCRIT));
    }
    
    boolean isStructureMembresAssocies(Structure s) {
    	boolean isMembresAssocies = (StructureUtils.isTypeMembresAssocies(s.typeStructure) || StructureUtils.isTypeMembresAssociesTerritorial(s.typeStructure));
    	return isMembresAssocies;
    }
}
