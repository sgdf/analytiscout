package fr.sgdf.analytiscout.dev.web;

import java.util.List;

import javax.ws.rs.WebApplicationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.sgdf.analytiscout.dev.modele.StructureHie;
import fr.sgdf.analytiscout.dev.modele.structureRequest;
import fr.sgdf.analytiscout.dev.service.IntranetCache;
import fr.sgdf.analytiscout.dev.service.StructureService;
import fr.sgdf.intranetapi.modeles.structure.Structure;
import redis.clients.jedis.Jedis;

@RestController
@RequestMapping("/api/analytiscout/structures")
public class ApiStructures extends fr.sgdf.analytiscout.dev.UserExtraUtils {
	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final Logger log = LoggerFactory.getLogger(ApiStructures.class);

	final private IntranetCache cacheUtilisateurs;
	final private StructureService structureService;

	public ApiStructures(StructureService structureService, IntranetCache cacheUtilisateurs) {
		this.cacheUtilisateurs = cacheUtilisateurs;
		this.structureService = structureService;
	}

	@PostMapping(value = "/structures")
	public List<Structure> structures(@RequestBody structureRequest structureFonctions) {
		String login = getConnectedLogin();
		try (Jedis cache = cacheUtilisateurs.get()) {
			return structureService.structures(cache, login, structureFonctions);
		} catch (Exception e) {
			log.error("structures", e);
		}
		return null;
	}

	@GetMapping(value = "/structureHie/{id}")
	public StructureHie structuresHie(@PathVariable(name = "id") Integer id) {
		String login = getConnectedLogin();
		try (Jedis cache = cacheUtilisateurs.get()) {
			return structureService.structuresHie(cache, login, id);
		} catch (Exception e) {
			throw new WebApplicationException(e);
		}
	}

	@GetMapping(value = "/structureHie/{id}/full")
	public List<StructureHie> structuresHieFull(@PathVariable(name = "id") Integer id) {
		String login = getConnectedLogin();
		try (Jedis cache = cacheUtilisateurs.get()) {
			return structureService.structuresHies(cache, login, id);
		} catch (Exception e) {
			throw new WebApplicationException(e);
		}
	}

	@PostMapping(value = "/structuresHie/{jeunes}")
	public List<StructureHie> structuresHie(@RequestBody structureRequest structureFonctions,
			@PathVariable(name = "jeunes") Boolean jeunes) {
		String login = getConnectedLogin();
		try (Jedis cache = cacheUtilisateurs.get()) {
			return structureService.structuresHie(cache, login, structureFonctions, jeunes);
		} catch (Exception e) {
			throw new WebApplicationException(e);
		}
	}
}
