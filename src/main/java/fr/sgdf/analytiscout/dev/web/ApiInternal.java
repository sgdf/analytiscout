package fr.sgdf.analytiscout.dev.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.sgdf.analytiscout.config.ApplicationProperties;
import fr.sgdf.analytiscout.dev.service.IntranetCache;
import redis.clients.jedis.Jedis;

@RestController
@RequestMapping("/api/analytiscout")
public class ApiInternal {
	private final Logger log = LoggerFactory.getLogger(ApiAdherents.class);
	final private IntranetCache cacheUtilisateurs;

	public ApiInternal(IntranetCache cacheUtilisateurs, ApplicationProperties appProperties) {
		super();
        this.cacheUtilisateurs = cacheUtilisateurs;
    }
    
    @PostMapping(value = "/tadherent/{codeAdherent}/{tcodeAdherent}/{duree}")
    public void setTadherent(@PathVariable Long codeAdherent, @PathVariable Long tcodeAdherent, @PathVariable Long duree) {
    	log.info("REST request to set tadherent {} -> {} : {} secondes", codeAdherent, tcodeAdherent, duree);
    	try (Jedis cache = cacheUtilisateurs.get()) {
    		cacheUtilisateurs.setTranslateAdherent(cache, codeAdherent, tcodeAdherent, duree);
    	}
    }
    
    @GetMapping(value = "/tadherent/{codeAdherent}/{tcodeAdherent}/{duree}")
    public void setTadherent2(@PathVariable Long codeAdherent, @PathVariable Long tcodeAdherent, @PathVariable Long duree) {
    	log.info("REST request to set tadherent {} -> {} : {} secondes", codeAdherent, tcodeAdherent, duree);
    	try (Jedis cache = cacheUtilisateurs.get()) {
    		cacheUtilisateurs.setTranslateAdherent(cache, codeAdherent, tcodeAdherent, duree);
    	}
    }
    
    @GetMapping(value = "/tadherent/{codeAdherent}")
    public long getTadherent(@PathVariable Long codeAdherent) {
    	log.info("REST request to get tadherent {}", codeAdherent);
    	try (Jedis cache = cacheUtilisateurs.get()) {
    		return cacheUtilisateurs.getTranslateAdherent(cache, codeAdherent);
    	}
    }
    
    @DeleteMapping(value = "/tadherent/{codeAdherent}")
    public void delTadherent(@PathVariable Long codeAdherent) {
    	log.info("REST request to delete tadherent {}", codeAdherent);
    	try (Jedis cache = cacheUtilisateurs.get()) {
    		cacheUtilisateurs.deleteTranslateAdherent(cache, codeAdherent);
    	}
    }
}
