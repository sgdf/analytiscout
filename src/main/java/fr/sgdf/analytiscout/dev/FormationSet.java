package fr.sgdf.analytiscout.dev;

import java.util.TreeSet;

import fr.sgdf.analytiscout.dev.modele.Formation;

public class FormationSet extends TreeSet<Formation> {

	public Formation getT(String nom) {
		for (Formation value : this) {
			if (value.formateur == false && value.tType.compareTo(nom) == 0)
				return value;
		}
		return null;
	}
	
	public Formation get(String nom) {
		for (Formation value : this) {
			if (value.formateur == false && value.type.compareTo(nom) == 0)
				return value;
		}
		return null;
	}
}
