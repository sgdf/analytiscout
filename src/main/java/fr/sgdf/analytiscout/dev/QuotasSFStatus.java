package fr.sgdf.analytiscout.dev;

public class QuotasSFStatus {

    public boolean directeur = true;
    public int directeurEff = 0;
    public int directeurNb = 0;
    public boolean encadrantMinimum = true;
    public int encadrantMinimumEff = 0;
    public int encadrantMinimumNb = 0;
    public boolean qualifie = true;
    public int qualifieEff = 0;
    public int qualifieNb = 0;
    public boolean stagiaire = true;
    public int stagiaireEff = 0;
    public String stagiaireNb;
    public boolean nonQualifie = true;
    public int nonQualifieEff = 0;
    public String nonQualifieNb;

    public String toString() {
        return (
            "directeur_=" +
            directeur +
            ",encadrantMinimum_=" +
            encadrantMinimum +
            ",qualifie_=" +
            qualifie +
            ",stagiaire_=" +
            stagiaire +
            ",nonQualifie_=" +
            nonQualifie
        );
    }
}
