package fr.sgdf.analytiscout.dev.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import fr.sgdf.analytiscout.config.ApplicationProperties;
import fr.sgdf.analytiscout.dev.Utils;
import fr.sgdf.analytiscout.dev.modele.StructureHie;
import fr.sgdf.analytiscout.dev.modele.structureRequest;
import fr.sgdf.intranetapi.exceptions.ApiException;
import fr.sgdf.intranetapi.exceptions.ApiSessionException;
import fr.sgdf.intranetapi.exceptions.ApiSessionExhaustedPoolException;
import fr.sgdf.intranetapi.modeles.adherent.Adherent;
import fr.sgdf.intranetapi.modeles.adherent.Branche;
import fr.sgdf.intranetapi.modeles.structure.Structure;
import fr.sgdf.intranetapi.utils.StructureUtils;
import redis.clients.jedis.Jedis;

@Service
public class StructureService {
	private final Logger log = LoggerFactory.getLogger(StructureService.class);

	final private IntranetCache cacheUtilisateurs;
	final private ApplicationProperties appProperties;

	public StructureService(ApplicationProperties appProperties, IntranetCache cacheUtilisateurs) {
		this.cacheUtilisateurs = cacheUtilisateurs;
		this.appProperties = appProperties;
		
		try (Jedis cache = cacheUtilisateurs.get()) {
			cacheUtilisateurs.getStructure(cache);
		}
	}
	
    @Scheduled(fixedRate=1, timeUnit = TimeUnit.HOURS)
    public void preloadStructures() {
    	try (Jedis cache = cacheUtilisateurs.get()) {
			cacheUtilisateurs.getStructure(cache);
		}
    }

	public List<Structure> structures(Jedis jedis, String login, structureRequest structureFonctions) throws NumberFormatException, IOException, ApiSessionException, ApiException, ApiSessionExhaustedPoolException {
		String impersonatedLogin = cacheUtilisateurs.translateAdherent(jedis, login);

		log.info("structures");
		Adherent adherent = cacheUtilisateurs.getAdherentFull(jedis, Long.valueOf(impersonatedLogin));
		int codeStructure = structureFonctions.structureFonctions != null
				? structureFonctions.structureFonctions.codeStructure
				: adherent.fonctionPrincipale.structure.code;

		List<Structure> structuresApi = cacheUtilisateurs.getStructure(jedis);
		List<Structure> structures = structuresApi.stream()
				.sorted(Comparator.comparingInt(Structure::getCodeStructure))
				.filter(s -> Utils.getCodeterritoire(codeStructure, s.codeStructure)).collect(Collectors.toList());
		if (appProperties.getIntranet().isAnonymiser()) {
			structures.forEach(structure -> anonymiserStructure(structure));
		}
		return structures;
	}
	
	private static void scanHie(List<Structure> datas, List<StructureHie> items, Boolean jeunes) {
		Map<Integer, StructureHie> s = new TreeMap<Integer, StructureHie>();
		List<StructureHie> l = new ArrayList<StructureHie>();
		
		// National
		datas.forEach(data -> {
			if (data.isTypeNational()) {
				StructureHie h = new StructureHie(data);
				l.add(h);
				s.put(l.size()-1, h);
				items.add(h);
			}
		});
		
		// Territoire
		datas.forEach(data -> {
			if (data.isEchelonTerritorial()) {
				StructureHie h = new StructureHie(data);
				l.add(h);
				s.put(l.size()-1, h);
				items.add(h);
			}
		});
		
		// Groupe
		final boolean sWasEmptyGroupe = s.isEmpty();
		datas.forEach(data -> {
			if (data.isTypeGroupe()) {
				StructureHie h = new StructureHie(data);
				
				if (sWasEmptyGroupe) {
					l.add(h);
					s.put(l.size()-1, h);
					items.add(h);
				}
				else
				{
					int terr = StructureUtils.getCodeTerritoire(data.getCodeStructure());
					s.forEach((k,v) -> {
						if (terr == v.codeStructure) {
							v.children.add(h);
							return;
						}
					});
					l.add(h);
					s.put(l.size()-1, h);
				}
			}
		});
		
		// Unite
		final boolean sWasEmptyUnite = s.isEmpty();
		datas.forEach(data -> {
			if (data.isTypeUnite()) {
				StructureHie h = new StructureHie(data);
				
				boolean toAdd = !(jeunes != null && jeunes == true && data.branche.name().compareTo(Branche.COMPAGNON.name()) == 0);
				
				if (sWasEmptyUnite) {
					l.add(h);
					s.put(l.size()-1, h);
					if (toAdd) items.add(h);
				}
				else {
					int groupe = StructureUtils.getCodeGroupe(data.getCodeStructure());
					s.forEach((k,v) -> {
						if (groupe == v.codeStructure) {
							if (toAdd) v.children.add(h);
							return;
						}
					});
					l.add(h);
					s.put(l.size()-1, h);
				}
			}
		});
	}
	
	public static boolean compareCode(Integer codeStructureAdherent, int codeStructure)
	{
		return codeStructureAdherent == codeStructure;
	}

	public StructureHie structuresHie(Jedis jedis, String login, Integer id) {
		log.info("structureHie {} by login {}", id, login);
		List<Structure> structuresApi = cacheUtilisateurs.getStructure(jedis);
		List<Structure> structuresFiltered = structuresApi.stream()
				.sorted(Comparator.comparingInt(Structure::getCodeStructure))
				.filter(s -> compareCode(id, s.codeStructure)).collect(Collectors.toList());
		List<StructureHie> structures = new ArrayList<StructureHie>();
		scanHie(structuresFiltered, structures, null);
		if (structures.size() == 1) {
			return new StructureHie(structures.get(0));
		}
		return null;
	}

	public List<StructureHie> structuresHies(Jedis jedis, String login, Integer id) {
		log.info("structureHie {} by login {}", id, login);
		List<Structure> structuresApi = cacheUtilisateurs.getStructure(jedis);
		List<Structure> structuresFiltered = structuresApi.stream()
				.sorted(Comparator.comparingInt(Structure::getCodeStructure))
				.filter(s -> Utils.getCodeterritoire(id, s.codeStructure)).collect(Collectors.toList());
		List<StructureHie> structures = new ArrayList<StructureHie>();
		scanHie(structuresFiltered, structures, null);
		return structures;
	}

	public List<StructureHie> structuresHie(Jedis jedis, String login, structureRequest structureFonctions, Boolean jeunes) throws NumberFormatException, IOException, ApiSessionException, ApiException {
		if (!login.matches("\\d+")) {
			return Collections.emptyList();
		}
		String impersonatedLogin = cacheUtilisateurs.translateAdherent(jedis, login);

		log.info("structuresHie by login {}", login);
		Adherent adherent = cacheUtilisateurs.getAdherentFull(jedis, Long.valueOf(impersonatedLogin));
		int codeStructure = structureFonctions.structureFonctions != null
				? structureFonctions.structureFonctions.codeStructure
				: adherent.fonctionPrincipale.structure.code;

		List<Structure> structuresApi = cacheUtilisateurs.getStructure(jedis);
		List<Structure> structuresFiltered = structuresApi.stream()
				.sorted(Comparator.comparingInt(Structure::getCodeStructure))
				.filter(s -> Utils.getCodeterritoire(codeStructure, s.codeStructure)).collect(Collectors.toList());
		
		List<StructureHie> structures = new ArrayList<StructureHie>();
		scanHie(structuresFiltered, structures, jeunes);
		if (appProperties.getIntranet().isAnonymiser()) {
			structures.forEach(structure -> anonymiserStructure(structure));
		}
		if(structures.size() > 0) {
			StructureHie fstructure = structures.get(0);
			if (fstructure.children.size() > 0) {
				structures.add(0, new StructureHie(fstructure));
				fstructure.label = "Structures dépendantes";
				fstructure.codeStructure = 0;
			}
		}
		return structures;
	}

	public List<Structure> structuresFonctions(Jedis jedis, String login) throws NumberFormatException, IOException, ApiSessionException, ApiException {
		if (!login.matches("\\d+")) {
			return Collections.emptyList();
		}
		String impersonatedLogin = cacheUtilisateurs.translateAdherent(jedis, login);

		List<Structure> structures = new ArrayList<Structure>();

		log.info("structuresFonctions");
		Adherent adherent = cacheUtilisateurs.getAdherentFull(jedis, Long.valueOf(impersonatedLogin));
		if (adherent != null) {
			int codeStructure = adherent.fonctionPrincipale.structure.code;
	
			Set<Integer> structuresF = new TreeSet<Integer>();
			structuresF.add(codeStructure);
			adherent.fonctionsSecondaires.forEach(f -> structuresF.add(f.structure.code));
	
			List<Structure> structuresApi = cacheUtilisateurs.getStructure(jedis);
			List<Structure> structuresFiltered = structuresApi.stream()
					.sorted(Comparator.comparingInt(Structure::getCodeStructure))
					.filter(s -> structuresF.contains(s.codeStructure)).collect(Collectors.toList());
	
			structuresFiltered.forEach(structure -> {
				if (structure.codeStructure == codeStructure)
					structures.add(0, structure);
				else
					structures.add(structure);
			});
			if (appProperties.getIntranet().isAnonymiser()) {
				structures.forEach(structure -> anonymiserStructure(structure));
			}
		}
		return structures;
	}
	
	private void anonymiserStructure(StructureHie structure) {
		String unite = structure.label;
		if (unite.startsWith("TERRITOIRE ")) {
			unite = "TERRITOIRE " + "UNIVERS";
			structure.label = unite;
		} else if (unite.startsWith("GROUPE ")) {
			structure.label = "GROUPE "+structure.codeStructure;
		} else if (StructureUtils.isUnite(structure.codeStructure)) {
			structure.label = "UNITE "+structure.codeStructure + " - " + structure.codeStructure;
		} else structure.label = "STRUCTURE AUTRE";
	}
	
	private void anonymiserStructure(Structure structure) {
		String unite = structure.nomStructure;
		if (unite.startsWith("TERRITOIRE ")) {
			unite = "TERRITOIRE " + "UNIVERS";
			structure.nomStructure = unite;
		} else if (unite.startsWith("GROUPE ")) {
			structure.nomStructure = "GROUPE "+structure.codeStructure;
		} else if (structure.isEchelonLocale()) {
			structure.nomStructure = "UNITE "+structure.branche.name() + " - " + structure.codeStructure;
		} else structure.nomStructure = "STRUCTURE AUTRE";
	}
}
