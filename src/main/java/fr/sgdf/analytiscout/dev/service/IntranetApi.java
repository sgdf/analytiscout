package fr.sgdf.analytiscout.dev.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import fr.sgdf.analytiscout.config.ApplicationProperties;
import fr.sgdf.intranetapi.ApiCall;
import fr.sgdf.intranetapi.ApiCallFactory;
import fr.sgdf.intranetapi.ApiTokenRequester;
import fr.sgdf.intranetapi.exceptions.ApiSessionException;
import fr.sgdf.intranetapi.exceptions.ApiTokenException;
import fr.sgdf.intranetapi.modeles.token.Token;
import io.micrometer.core.instrument.MeterRegistry;
import tech.jhipster.config.JHipsterProperties;

@Service
public class IntranetApi {

    private final Logger log = LoggerFactory.getLogger(IntranetApi.class);

    private ApiTokenRequester apiRequester_;
    private ApiCallFactory api_;
    private Token token_;
    private Long tokenLock_ = Long.valueOf(1);

    private String clientIdKeycloak = null;
    private String secretKeycloak = null;
    private String urlAuth_ = null;
    private String url_ = null;
    private String realms_ = null;
    private final MeterRegistry registry_;

    public IntranetApi(ApplicationProperties appProperties, JHipsterProperties jhipsterProperties, MeterRegistry registry) {
        urlAuth_ = appProperties.getIntranet().getUrlAuth();
        url_ = appProperties.getIntranet().getUrl();
        realms_ = appProperties.getIntranet().getRealms();
        clientIdKeycloak = appProperties.getIntranet().getClientIdKeycloak();
        secretKeycloak = appProperties.getIntranet().getSecretKeycloak();
        this.registry_ = registry;
        try {
            this.api_ = new ApiCallFactory(appProperties.getIntranet().getClientId(), url_);
        	this.api_.setRegistry(registry_);
            this.apiRequester_ = new ApiTokenRequester(urlAuth_, realms_, clientIdKeycloak, secretKeycloak);
            token_ = this.apiRequester_.getToken(null);
        } catch (ApiTokenException e) {
            log.error("erreur ApiSessionManager", e);
        } catch (Exception e) {
            log.error("erreur ApiSessionManager", e);
        }

        log.info("Intranet : Utilisation de la base \"{}\"", url_);
    }

    public ApiCall createCall() throws ApiSessionException, ApiTokenException {
        synchronized (tokenLock_) {
        	token_ = apiRequester_.getToken(token_);
            return this.api_.allocate(token_.token_type, token_.access_token);
        }
    }

    public ApiCallFactory getApiFactory() {
        return api_;
    }
    
    @Scheduled(initialDelay = 60*1000, fixedRate = 600*1000)
    public void renewToken() {
    	synchronized (tokenLock_) {
        	try {
				token_ = apiRequester_.getToken(token_);
			} catch (ApiTokenException e) {
	            log.error("erreur renewToken", e);
				token_ = null;
			}
    	}
    }
}
