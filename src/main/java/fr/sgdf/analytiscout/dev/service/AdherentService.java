package fr.sgdf.analytiscout.dev.service;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.sgdf.analytiscout.config.ApplicationProperties;
import fr.sgdf.analytiscout.dev.modele.Responsable;
import fr.sgdf.intranetapi.exceptions.ApiException;
import fr.sgdf.intranetapi.modeles.adherent.Adherent;
import redis.clients.jedis.Jedis;

@Service
public class AdherentService {
	private final Logger log = LoggerFactory.getLogger(AdherentService.class);

	final private IntranetCache cacheUtilisateurs;
	final private ApplicationProperties appProperties;

	public AdherentService(ApplicationProperties appProperties, IntranetCache cacheUtilisateurs) {
		this.cacheUtilisateurs = cacheUtilisateurs;
		this.appProperties = appProperties;
	}

	public Responsable getCurrentAdherentFull(Jedis cache, String login) throws NumberFormatException, IOException, ApiException {
		if (cache == null) {
			return null;
		}
		String impersonatedLogin = cacheUtilisateurs.translateAdherent(cache, login);
		log.info("REST request to get adherent {}", impersonatedLogin);
		Adherent adherent = cacheUtilisateurs.getAdherentFull(cache, Long.valueOf(impersonatedLogin));

		Responsable responsable = new Responsable();
		responsable.transform(adherent);
		if (appProperties.getIntranet().isAnonymiser()) responsable.anonymiser();
		return responsable;
	}
}
