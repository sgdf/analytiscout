package fr.sgdf.analytiscout.dev.service;

import java.util.List;
import java.util.Set;

import fr.sgdf.analytiscout.dev.modele.Responsable;
import fr.sgdf.analytiscout.service.dto.AdminUserDTO;
import fr.sgdf.intranetapi.modeles.structure.Structure;

public class UserVMExtended extends AdminUserDTO {
	public List<Structure> structuresFonctions;
	public Responsable adherent;

	public UserVMExtended() {
		super();
	}

	public UserVMExtended(String login, Set<String> authorities) {
		super();
	}
}
