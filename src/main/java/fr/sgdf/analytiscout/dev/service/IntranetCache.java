package fr.sgdf.analytiscout.dev.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.sgdf.analytiscout.config.ApplicationProperties;
import fr.sgdf.analytiscout.dev.StopWatch;
import fr.sgdf.intranetapi.ApiCall;
import fr.sgdf.intranetapi.ApiParser;
import fr.sgdf.intranetapi.exceptions.ApiException;
import fr.sgdf.intranetapi.modeles.adherent.Adherent;
import fr.sgdf.intranetapi.modeles.adherent.diplome.Diplome;
import fr.sgdf.intranetapi.modeles.adherent.formation.Formation;
import fr.sgdf.intranetapi.modeles.adherent.qualification.Qualification;
import fr.sgdf.intranetapi.modeles.structure.StructureRequest;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

@Service
public class IntranetCache {

	private final Logger log = LoggerFactory.getLogger(IntranetCache.class);

	private JedisPool jedisPool = null;

	@Autowired
	private IntranetApi apiManager;

	private long expire = -1;
	private long expires = -1;

	private static final String PREFIX = "analytiscout:";
	private static final String ADHERENTS = "adherents/";
	private static final String FORMATIONS = "formations/";
	private static final String QUALIFICATIONS = "qualifications/";
	private static final String DIPLOMES = "diplomes/";
	private static final String TADHERENTS = "tadherents/";

	public IntranetCache(ApplicationProperties appProperties) {
		if (appProperties.getCacheUtilisateurs().getHost().isEmpty() == false) {
			Integer db = appProperties.getCacheUtilisateurs().getDb();
			jedisPool = new JedisPool(new JedisPoolConfig(), appProperties.getCacheUtilisateurs().getHost(),
					Integer.valueOf(appProperties.getCacheUtilisateurs().getPort()), Protocol.DEFAULT_TIMEOUT,
					appProperties.getCacheUtilisateurs().getAuth().isEmpty() == false ? appProperties.getCacheUtilisateurs().getAuth() : null, db, false);
			String p = appProperties.getCacheUtilisateurs().getExpire();
			if (p != null) {
				expire = Integer.parseInt(p);
			}
			p = appProperties.getCacheUtilisateurs().getExpires();
			if (p != null) {
				expires = Integer.parseInt(p);
			}
			log.info("IntranetCache on {}:{} created", appProperties.getCacheUtilisateurs().getHost(),
					appProperties.getCacheUtilisateurs().getPort());
		}
	}
	
	public Jedis get() {
		return jedisPool != null ? jedisPool.getResource() : null;
	}
	
	public String translateAdherent(Jedis jedis, String codeAdherent) {
		String key = PREFIX + TADHERENTS + codeAdherent;
		String tcodeAdherent = jedis.get(key);
		return tcodeAdherent == null ? codeAdherent : tcodeAdherent;
	}

	public void setTranslateAdherent(Jedis jedis, Long codeAdherent, Long tcodeAdherent, Long duree) {
		String key = PREFIX + TADHERENTS + codeAdherent;
		jedis.setex(key, duree, tcodeAdherent.toString());
	}
	
	public Long translateAdherent(Jedis jedis, Long codeAdherent) {
		String key = PREFIX + TADHERENTS + codeAdherent;
		String tcodeAdherent = jedis.get(key);
		return tcodeAdherent == null ? codeAdherent : Long.valueOf(tcodeAdherent);
	}

	public long getTranslateAdherent(Jedis jedis, Long codeAdherent) {
		String key = PREFIX + TADHERENTS + codeAdherent;
		long ttl = jedis.ttl(key);
		return ttl;
	}

	public void deleteTranslateAdherent(Jedis cache, Long codeAdherent) {
		String key = PREFIX + TADHERENTS + codeAdherent;
		cache.del(key);
	}
	
	public List<fr.sgdf.intranetapi.modeles.structure.Structure> getStructure(Jedis jedis) {
		List<fr.sgdf.intranetapi.modeles.structure.Structure> structures = new ArrayList<fr.sgdf.intranetapi.modeles.structure.Structure>();
		log.info("getStructure");

		StopWatch sw = new StopWatch();
		String key = PREFIX + "structures";
		String structuresJson = jedis.get(key);
		if (structuresJson == null) {
			StructureRequest request = new StructureRequest();
			try {
				ApiCall api = apiManager.createCall();
				structures = api.structures(request);
			}
			catch (Throwable e) {
				log.error("getStructure", e);
			}
			
			try {
				structuresJson = ApiParser.encodeStructures(structures);
			} catch (ApiException e) {
				log.error("getStructure", e);
			}
			jedis.setex(key, expires, structuresJson);
		}
		else {
			try {
				structures = ApiParser.parseStructures(structuresJson);
			} catch (ApiException e) {
				log.error("getStructure", e);
			}
		}
		log.info("getStructure in {} milliseconds", sw.deltaMilli());
		return structures;
	}

	public Adherent getAdherentFull(Jedis jedis, String login) throws IOException, ApiException {
		Long id = Long.parseLong(login);
		return getAdherentFull(jedis, id);
	}

	public Collection<Adherent> getAdherentsFull(Jedis jedis, String login, List<Long> ids)
			throws IOException, ApiException {
		Collection<Adherent> adherents = new ArrayList<Adherent>();
		if (ids.isEmpty() == true) return adherents;
		log.info("getAdherentFull {} by {}", ids.size(), login);

		StopWatch sw = new StopWatch();
		Map<Long, Adherent> datas = new TreeMap<Long, Adherent>();

		List<String> keys = new ArrayList<String>();
		for (int i = 0; i < ids.size(); i++) {
			String key = PREFIX + ADHERENTS + ids.get(i);
			keys.add(key);
		}
		List<Long> rq = new ArrayList<Long>();
		List<String> results = jedis.mget(keys.toArray(new String[0]));
		for (int i = 0; i < ids.size(); i++) {
			String v = results.get(i);
			if (v != null) {
				Adherent adherentObj = ApiParser.parseAdherent(v);
				datas.put(ids.get(i), adherentObj);
			} else {
				rq.add(ids.get(i));
			}
		}
		if (rq.isEmpty() == false) {
			List<Adherent> ads = null;
			try {
				ApiCall api = apiManager.createCall();
    			ads = api.adherents(rq);
			}
			catch (Throwable e) {
				log.error("getAdherentFull", e);
			}
			ads.forEach(ad -> {
				String key = PREFIX + ADHERENTS + ad.codeAdherent;
				try {
					String adherentJson = ApiParser.encodeAdherent(ad);
					if (expire != 0)
						jedis.setex(key, expire, adherentJson);
					else
						jedis.set(key, adherentJson);
				} catch (ApiException e) {
				}
				datas.put(ad.codeAdherent, ad);
			});
		}
		adherents = datas.values();
		log.info("getAdherentFull {} by {} in {} milliseconds", ids.size(), login, sw.deltaMilli());
		return adherents;
	}

	public Adherent getAdherentFull(Jedis jedis, Long id) throws IOException, ApiException {
		log.info("getAdherentFull {}", id);
		StopWatch sw = new StopWatch();
		
		Adherent adherentObj = null;
		if (jedis == null) {
			return null;
		}
		String key = PREFIX + ADHERENTS + id;
		String adherentJson = jedis.get(key);
		if (adherentJson == null) {
			try {
				ApiCall api = apiManager.createCall();
				adherentJson = api.adherents_brut(id);
				if (expire != 0)
					jedis.setex(key, expire, adherentJson);
				else
					jedis.set(key, adherentJson);
			}
			catch (Throwable e) {
				log.error("getAdherentFull", e);
			}
		}
		adherentObj = ApiParser.parseAdherent(adherentJson);
		log.info("getAdherentFull {} in {} milliseconds", id, sw.deltaMilli());
		return adherentObj;
	}

	public List<Adherent> findAllFull(Jedis jedis, String login, List<Long> ids) throws Exception {
		List<Adherent> adherents = new ArrayList<Adherent>();
		if (!ids.isEmpty()) {
			Collection<Adherent> adherentsFromCache = getAdherentsFull(jedis, login, ids);
			adherentsFromCache.forEach(adherent -> {
				adherents.add(adherent);
			});
		}
		return adherents;
	}

	public Map<Long, List<Qualification>> getQualificationsFull(Jedis jedis, List<Long> ids) {
		final Map<Long, List<Qualification>> dataMap = new TreeMap<Long, List<Qualification>>();
		if (ids.isEmpty() == true) return dataMap;
		log.info("getQualificationsFull {}", ids.size());

		StopWatch sw = new StopWatch();
		
		List<String> keys = new ArrayList<String>();
		for (int i = 0; i < ids.size(); i++) {
			String key = PREFIX + QUALIFICATIONS + ids.get(i);
			keys.add(key);
		}
		List<Long> rq = new ArrayList<Long>();
		List<String> results = jedis.mget(keys.toArray(new String[0]));
		for (int i = 0; i < ids.size(); i++) {
			String jsonData = results.get(i);
			if (jsonData != null) {
				List<Qualification> list = null;
				try {
					list = Arrays.asList(ApiParser.parseQualifications(jsonData));
				}
				catch (Throwable e) {
					log.error("getQualificationsFull", e);
				}
				List<Qualification> lData = dataMap.computeIfAbsent(ids.get(i), map -> new ArrayList<Qualification>());
				lData.addAll(list);
			} else {
				rq.add(ids.get(i));
			}
		}
		if (rq.isEmpty() == false) {
			Map<Long, List<Qualification>> dataMapSrv = null;
			
			try {
				ApiCall api = apiManager.createCall();
    			dataMapSrv = api.qualifications(rq);
			}
			catch (Throwable e) {
				log.error("getQualificationsFull", e);
			}
			dataMapSrv.forEach((codeAdherent, mdata) -> {
				String key = PREFIX + QUALIFICATIONS + codeAdherent;
				try {
					String jsonData = ApiParser.encodeQualifications(mdata);
					if (expire != 0)
						jedis.setex(key, expire, jsonData);
					else
						jedis.set(key, jsonData);
					List<Qualification> lData = dataMap.computeIfAbsent(codeAdherent, map -> new ArrayList<Qualification>());
					lData.addAll(mdata);
				} catch (ApiException e) {
				}
			});
		}
		log.info("getQualificationsFull {} in {} milliseconds", ids.size(), sw.deltaMilli());
		return dataMap;
	}

	public Map<Long, List<Formation>> getFormationsFull(Jedis jedis, List<Long> ids) {
		final Map<Long, List<Formation>> dataMap = new TreeMap<Long, List<Formation>>();
		if (ids.isEmpty() == true) return dataMap;
		log.info("getFormationsFull {}", ids.size());

		StopWatch sw = new StopWatch();
		
		List<String> keys = new ArrayList<String>();
		for (int i = 0; i < ids.size(); i++) {
			String key = PREFIX + FORMATIONS + ids.get(i);
			keys.add(key);
		}
		List<Long> rq = new ArrayList<Long>();
		List<String> results = jedis.mget(keys.toArray(new String[0]));
		for (int i = 0; i < ids.size(); i++) {
			String jsonData = results.get(i);
			if (jsonData != null) {
				List<Formation> list = null;
				try {
					list = Arrays.asList(ApiParser.parseFormations(jsonData));
				}
				catch (Throwable e) {
					log.error("getFormationsFull", e);
				}
				List<Formation> lData = dataMap.computeIfAbsent(ids.get(i), map -> new ArrayList<Formation>());
				lData.addAll(list);
			} else {
				rq.add(ids.get(i));
			}
		}
		if (rq.isEmpty() == false) {
			Map<Long, List<Formation>> dataMapSrv = null;
			try {
				ApiCall api = apiManager.createCall();
    			dataMapSrv = api.formations(rq);
			}
			catch (Throwable e) {
				log.error("getFormationsFull", e);
			}
			dataMapSrv.forEach((codeAdherent, mdata) -> {
				String key = PREFIX + FORMATIONS + codeAdherent;
				try {
					String jsonData = ApiParser.encodeFormations(mdata);
					if (expire != 0)
						jedis.setex(key, expire, jsonData);
					else
						jedis.set(key, jsonData);
					List<Formation> lData = dataMap.computeIfAbsent(codeAdherent, map -> new ArrayList<Formation>());
					lData.addAll(mdata);
				} catch (ApiException e) {
				}
			});
		}
		log.info("getFormationsFull {} in {} milliseconds", ids.size(), sw.deltaMilli());
		return dataMap;
	}

	public Map<Long, List<Diplome>> getDiplomesFull(Jedis jedis, List<Long> ids) {
		final Map<Long, List<Diplome>> dataMap = new TreeMap<Long, List<Diplome>>();
		if (ids.isEmpty() == true) return dataMap;
		log.info("getDiplomesFull {}", ids.size());

		StopWatch sw = new StopWatch();
		
		List<String> keys = new ArrayList<String>();
		for (int i = 0; i < ids.size(); i++) {
			String key = PREFIX + DIPLOMES + ids.get(i);
			keys.add(key);
		}
		List<Long> rq = new ArrayList<Long>();
		List<String> results = jedis.mget(keys.toArray(new String[0]));
		for (int i = 0; i < ids.size(); i++) {
			String jsonData = results.get(i);
			if (jsonData != null) {
				List<Diplome> list = null;
				try {
					list = Arrays.asList(ApiParser.parseDiplomes(jsonData));
				}
				catch (Throwable e) {
					log.error("getDiplomesFull", e);
				}
				List<Diplome> lData = dataMap.computeIfAbsent(ids.get(i), map -> new ArrayList<Diplome>());
				lData.addAll(list);
			} else {
				rq.add(ids.get(i));
			}
		}
		if (rq.isEmpty() == false) {
			Map<Long, List<Diplome>> dataMapSrv = null;
			try {
				ApiCall api = apiManager.createCall();
    			dataMapSrv = api.diplomes(rq);
			}
			catch (Throwable e) {
				log.error("getDiplomesFull", e);
			}
			dataMapSrv.forEach((codeAdherent, mdata) -> {
				String key = PREFIX + DIPLOMES + codeAdherent;
				try {
					String jsonData = ApiParser.encodeDiplomes(mdata);
					if (expire != 0)
						jedis.setex(key, expire, jsonData);
					else
						jedis.set(key, jsonData);
					List<Diplome> lData = dataMap.computeIfAbsent(codeAdherent, map -> new ArrayList<Diplome>());
					lData.addAll(mdata);
				} catch (ApiException e) {
				}
			});
		}
		log.info("getDiplomesFull {} in {} milliseconds", ids.size(), sw.deltaMilli());
		return dataMap;
	}
}
