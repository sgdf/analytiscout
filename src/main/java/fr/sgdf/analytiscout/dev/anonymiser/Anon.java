package fr.sgdf.analytiscout.dev.anonymiser;

public class Anon {

	private static Anonymizer anonymiser;
	static {
		anonymiser = new Anonymizer();
		anonymiser.init();
	}
	
	public static Anonymizer getAnonymizer() {
		return anonymiser;
	}
}
