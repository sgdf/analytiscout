package fr.sgdf.analytiscout.dev;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import fr.sgdf.analytiscout.security.SecurityUtils;

public class UserExtraUtils {
    public UserExtraUtils() {
    }

	protected String getConnectedLogin() {
		String currentLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilisateur non-connecté à l'intranet"));
        return currentLogin;
	}
}
