package fr.sgdf.analytiscout.dev;

import com.google.common.collect.Lists;
import fr.sgdf.intranetapi.modeles.adherent.Branche;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class QuotaSF {

    public QuotaSF(int directeur, int encadrantMinimum, int qualifie, Integer stagiaire[], Integer nonQualifie[]) {
        directeur_ = directeur;
        encadrantMinimum_ = encadrantMinimum;
        qualifie_ = qualifie;
        stagiaire_ = stagiaire;
        nonQualifie_ = nonQualifie;
    }

    public Integer directeur_;
    public Integer encadrantMinimum_;
    public Integer qualifie_;
    public Integer stagiaire_[];
    public Integer nonQualifie_[];

    static Map<Integer, QuotaSF> quotas_ = new TreeMap<Integer, QuotaSF>();

    static {
        for (int i = 7; i <= 12; i++) quotas_.put(i, new QuotaSF(1, 1, 1, null, null));
        for (int i = 13; i <= 24; i++) quotas_.put(i, new QuotaSF(1, 2, 1, new Integer[] { 1 }, new Integer[] { 0 }));
        for (int i = 25; i <= 36; i++) quotas_.put(i, new QuotaSF(1, 3, 2, new Integer[] { 1, 0 }, new Integer[] { 1, 0 }));
        for (int i = 37; i <= 48; i++) quotas_.put(i, new QuotaSF(1, 4, 2, new Integer[] { 1, 2 }, new Integer[] { 1, 0 }));
        for (int i = 49; i <= 60; i++) quotas_.put(i, new QuotaSF(1, 5, 3, new Integer[] { 1, 2 }, new Integer[] { 1, 0 }));
        for (int i = 61; i <= 72; i++) quotas_.put(i, new QuotaSF(1, 6, 3, new Integer[] { 1, 2, 3 }, new Integer[] { 0, 1, 2 }));
        for (int i = 73; i <= 84; i++) quotas_.put(i, new QuotaSF(1, 7, 4, new Integer[] { 1, 2, 3 }, new Integer[] { 0, 1, 2 }));
        for (int i = 85; i <= 96; i++) quotas_.put(i, new QuotaSF(1, 8, 4, new Integer[] { 2, 3, 4 }, new Integer[] { 0, 1, 2 }));
        for (int i = 97; i <= 108; i++) quotas_.put(i, new QuotaSF(1, 9, 5, new Integer[] { 2, 3, 4 }, new Integer[] { 0, 1, 2 }));
    }

    public static QuotasSFStatus calcul(int jeunes, int chefs, Branche branche, int dir, int qualifs, int stagiaires, int nonQualifies) {
        QuotasSFStatus status = null;
        QuotaSF item = quotas_.get(jeunes);
        if (item != null) {
            boolean piok = branche == Branche.PIONNIER_CARAVELLE;
            status = new QuotasSFStatus();
            status.directeur = (dir == item.directeur_);
            status.directeurEff = dir;
            status.directeurNb = item.directeur_;
            status.encadrantMinimum = (chefs - (piok ? 0 : 1)) >= item.encadrantMinimum_;
            status.encadrantMinimumEff = (chefs - (piok ? 0 : 1));
            status.encadrantMinimumNb = item.encadrantMinimum_;
            status.qualifie = qualifs >= item.qualifie_;
            status.qualifieEff = qualifs;
            status.qualifieNb = item.qualifie_;
            if (item.stagiaire_ != null) {
                status.stagiaireEff = stagiaires;
                status.stagiaire = false;
                status.stagiaireNb =
                    Lists.newArrayList(item.stagiaire_).stream().map(n -> String.valueOf(n)).collect(Collectors.joining(" ou "));
                for (int st : item.stagiaire_) {
                    if (st == stagiaires) {
                        status.stagiaire = true;
                        break;
                    }
                }
            }
            if (item.nonQualifie_ != null) {
                status.nonQualifieEff = nonQualifies;
                status.nonQualifie = false;
                status.nonQualifieNb =
                    Lists.newArrayList(item.nonQualifie_).stream().map(n -> String.valueOf(n)).collect(Collectors.joining(" ou "));
                for (int nq : item.nonQualifie_) {
                    if (nq == nonQualifies) {
                        status.nonQualifie = true;
                        break;
                    }
                }
            }
        }
        return status;
    }
}
