package fr.sgdf.analytiscout.dev;

import java.time.Instant;

public class StopWatch {
	private Instant start = Instant.now();
	
	public long deltaMilli() {
		Instant end = Instant.now();
		return end.toEpochMilli() - start.toEpochMilli(); 
	}
}
