package fr.sgdf.analytiscout.dev.modele;

public class Alerte {
	public static final String ALERTE_TYPE_AGE = "Age";
	public static final String ALERTE_TYPE_QUALIFICATION = "Qualification";
	public static final String ALERTE_TYPE_INSCRIPTION = "Inscription";
	
	public enum Severite
	{
		INCONNUE,
		BASSE,
		MOYENNE,
		HAUTE;
	}
	
	public Alerte(Adherent adherent, Severite severite, String type, String message)
	{
		this.adherent = adherent;
		this.severite = severite;
		this.type = type;
		this.message = message;
		this.severite_classe = "severite_inconnue";
		if (this.severite == Severite.HAUTE) {
			this.severite_classe = "severite_haute";
		}
		if (this.severite == Severite.MOYENNE) {
			this.severite_classe = "severite_moyenne";
		}
		if (this.severite == Severite.BASSE) {
			this.severite_classe = "severite_basse";
		}
	}
	
	public String getTriKey() {
		return ""+(Severite.HAUTE.ordinal()-severite.ordinal())+"|"+this.adherent.codeStructure+"|"+this.adherent.codeAdherent;
	}
	
	public Adherent adherent;
	public String message;
	public String type;
	public Severite severite = Severite.INCONNUE;
	public String severite_classe;
	
	public int getSeveriteInt() {
		return this.severite.ordinal();
	}
}
