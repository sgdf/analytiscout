package fr.sgdf.analytiscout.dev.modele;

import java.text.SimpleDateFormat;
import java.util.Collection;

import fr.sgdf.analytiscout.dev.Utils;
import fr.sgdf.intranetapi.modeles.adherent.Branche;

public class Jeune extends Adherent {
	
	public Branche brancheAnneeProchaine;
	public String brancheAnneeProchaine_abreviation;
	public String brancheAnneeProchaine_classe;
	
	public void transform(fr.sgdf.intranetapi.modeles.adherent.Adherent adherent) {
		super.transform(adherent);
		branche = Utils.calculBrancheJeune(adherent.fonctionPrincipale.structure.code, adherent.dateDeNaissance);
		branche_classe = Utils.calculClasseBranche(branche);
		brancheAnneeProchaine = Utils.calculBrancheAnneeProchaineJeune(adherent.fonctionPrincipale.structure.code, adherent.dateDeNaissance);
		brancheAnneeProchaine_abreviation = Utils.calculAbreviationBranche(brancheAnneeProchaine);
		brancheAnneeProchaine_classe = Utils.calculClasseBranche(brancheAnneeProchaine);
	}
	
	public String getTriKey() {
		return ""+codeStructure+"|"+codeStructure+"|"+nom;
	}
	
	public boolean getAgeAnneeOk()
	{
		if (ageFinDec > 0)
		{
			switch (codeFonction)
			{
				case Adherent.CODE_FARFADETS:
					if (ageFinDec < 6) return false;
				break;
				case Adherent.CODE_LJ:
					if (ageFinDec < 8) return false;
				break;
				case Adherent.CODE_SG:
					if (ageFinDec < 11) return false;
				break;
				case Adherent.CODE_PIOK:
					if (ageFinDec < 14) return false;
				break;
			}
		}
		return true;
	}
	
	public boolean getAgeCampOk()
	{
		if (ageCamp > 0)
		{
			switch (codeFonction)
			{
				case Adherent.CODE_FARFADETS:
					if (ageCamp < 6) return false;
				break;
				case Adherent.CODE_LJ:
					if (ageCamp < 8) return false;
				break;
				case Adherent.CODE_SG:
					if (ageCamp < 11) return false;
				break;
				case Adherent.CODE_PIOK:
					if (ageCamp < 14) return false;
				break;
			}
		}
		return true;
	}
	
	public void complete() {
		super.complete();
	}

	public void genererAlertes(boolean calculAge, Collection<Alerte> alertes) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		switch (this.codeFonction)
		{
			case Adherent.CODE_FARFADETS:
				if (calculAge && this.age < 6)
					ajouterAlerte(alertes, this, Alerte.Severite.HAUTE, Alerte.ALERTE_TYPE_AGE, "Farfadet n'ayant pas encore 6 ans à ce jour, pas d'activité avant le "+simpleDateFormat.format(dateAge(6)));
			break;
			case Adherent.CODE_LJ:
				if (calculAge && this.getAgeAnneeOk() == false)
					ajouterAlerte(alertes, this, Alerte.Severite.MOYENNE, Alerte.ALERTE_TYPE_AGE, "Pas 8 ans au 31 décembre prochain, il/elle les aura le "+simpleDateFormat.format(dateAge(8)));
			break;
			case Adherent.CODE_SG:
				if (calculAge && this.getAgeCampOk() == false)
					ajouterAlerte(alertes, this, Alerte.Severite.HAUTE, Alerte.ALERTE_TYPE_AGE, "Pas 11 ans au 1er juillet prochain, il/elle les aura le "+simpleDateFormat.format(dateAge(11)));
				else if (calculAge && this.getAgeAnneeOk() == false)
					ajouterAlerte(alertes, this, Alerte.Severite.MOYENNE, Alerte.ALERTE_TYPE_AGE, "Pas 11 ans au 31 décembre prochain, il/elle les aura le "+simpleDateFormat.format(dateAge(11)));
			break;
			case Adherent.CODE_PIOK:
				if (calculAge && this.getAgeCampOk() == false)
					ajouterAlerte(alertes, this, Alerte.Severite.HAUTE, Alerte.ALERTE_TYPE_AGE, "Pas 14 ans au 1er juillet prochain, il/elle les aura le "+simpleDateFormat.format(dateAge(14)));
				else if (calculAge && this.getAgeAnneeOk() == false)
					ajouterAlerte(alertes, this, Alerte.Severite.MOYENNE, Alerte.ALERTE_TYPE_AGE, "Pas 14 ans au 31 décembre prochain, il/elle les aura le "+simpleDateFormat.format(dateAge(14)));
			break;
		}
	}
}
