package fr.sgdf.analytiscout.dev.modele;

import fr.sgdf.analytiscout.dev.DiplomeSet;
import fr.sgdf.analytiscout.dev.FormationSet;
import fr.sgdf.analytiscout.dev.Params;
import fr.sgdf.analytiscout.dev.Utils;
import fr.sgdf.intranetapi.modeles.adherent.Adherent.FonctionPrincipale;
import fr.sgdf.intranetapi.modeles.adherent.Statut;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Responsable extends Adherent {

    private static final Set<String> formationsFarfadets = new TreeSet<String>();
    private static final Set<String> formationsCompas = new TreeSet<String>();
    private static final Set<String> diplomesFarfadets = new TreeSet<String>();
    private static final Set<String> diplomesCompas = new TreeSet<String>();

    static {
        diplomesFarfadets.add("PSC1");
        diplomesFarfadets.add("AFPS");
        diplomesFarfadets.add("Sauveteur Secouriste du Travail");
        diplomesFarfadets.add("PSE1 Premiers Secours en Equipe de niveau 1");

        formationsFarfadets.add("PSC1");
        formationsFarfadets.add("PSC1");
        formationsFarfadets.add("Sauveteur Secouriste du Travail");
        formationsFarfadets.add("PSE1 Premiers Secours en Equipe de niveau 1");
        
        formationsCompas.add("FC2");
        formationsFarfadets.add("FORMATION DES RESPONSABLES FARFADETS");
        formationsFarfadets.add("FORMATION DES RESPONSABLES FARFADETS - 1");
        formationsFarfadets.add("FORMATION DES RESPONSABLES FARFADETS - 2");

        diplomesCompas.add("PSC1");
        formationsCompas.add("FC1");
        formationsCompas.add("FC2");
    }

    private static final String APF = "APF";
    private static final String APF_CHEFS_CHEFTAINES = "APF CHEFS-CHEFTAINES";
    private static final String APPRO = "APPRO";
    private static final String TECH = "TECH";
    private static final String ANIM_S = "ANIM_S";
    private static final String ANIM_T = "ANIM_T";
    private static final String ANIM = "ANIM";
    private static final String DIR = "DIR";
    private static final String DIR_S = "DIR_S";
    private static final String DIR_T = "DIR_T";
    private static final String ANIMATEUR_SF_CAFASF = "Animateur SF (CAFASF)";
    private static final String DIRECTEUR_SF_CAFDSF = "Directeur SF (CAFDSF)";

    private static final String PAS_DE_DATE = "Permanent";

    private static final String BAFA_FORMATION_GENERALE = "BAFA FORMATION GENERALE";

    public String codeFonctionSecondaireComplet;
    public String codeFonctionSecondaire_classe;
    public boolean ageDirecteur;
    public boolean age18ans1Juillet;
    public boolean compa;
    public boolean farfadet;
    public String fonctionSecondaire;

    public boolean bafapotentiel;

    public boolean apf;
    public boolean tech;
    public boolean appro;

    public String fonctionJS;
    public String diplomeJS;
    public String diplomeDetailJS;
    public String qualiteJS;
    public Date derniereModification;

    private Map<String, Qualification> pQualifications = new TreeMap<String, Qualification>();
    private DiplomeSet pDiplomesFarfadets = new DiplomeSet();
    private DiplomeSet pDiplomesCompas = new DiplomeSet();
    private FormationSet pFormationsFarfadets = new FormationSet();
    private FormationSet pFormationsCompas = new FormationSet();

    private Date convertToDateViaSqlTimestamp(LocalDateTime dateToConvert) {
        return java.sql.Timestamp.valueOf(dateToConvert);
    }

    private List<Formation> getFormationsPriv(Set<Formation> pFormations) {
        List<Formation> f = new ArrayList<Formation>();
        pFormations.forEach(value -> f.add(value));
        Collections.sort(f);
        return f;
    }

    public Collection<Formation> getFormationsFarfadets() {
        return getFormationsPriv(pFormationsFarfadets);
    }

    public Collection<Formation> getFormationsCompas() {
        return getFormationsPriv(pFormationsCompas);
    }

    public Collection<Formation> getFormations() {
        return getFormationsPriv(pFormations);
    }

    public Qualification getQualificationDir() {
        return pQualifications.get(DIR);
    }

    public Qualification getQualificationAnim() {
        return pQualifications.get(ANIM);
    }

    public Collection<Diplome> getDiplomeFarfadets() {
        return getDiplomesPriv(pDiplomesFarfadets);
    }

    public Collection<Diplome> getDiplomesCompas() {
        return getDiplomesPriv(pDiplomesCompas);
    }

    public String getTriKey() {
        return "" + codeStructure + "|" + codeStructure + "|" + nom;
    }

    public Formation getFormationCq() {
        return pFormations.get("CQ");
    }

    public Formation getFormationCf() {
        return pFormations.get("CF");
    }

    public Diplome getDiplomeCq() {
        return pDiplomes.get("CQ");
    }

    public Diplome getDiplomeCf() {
        return pDiplomes.get("CF");
    }

    public static final int CODE_RESPONSABLE_FARFADETS = 270;
    public static final int CODE_PARENTS_FARFADETS = 271;
    public static final int CODE_CHEFS_PIOK = 230;
    public static final int CODE_ACCOMPAGNATEUR_COMPAS = 240;
    public static final int CODE_RESPONSABLES = 200;
    public static final int CODE_IMPEESA = 180;

    public void nettoyage() {
        pQualifications.remove(DIR_T);
        pQualifications.remove(DIR_S);
        pQualifications.remove(ANIM_T);
        pQualifications.remove(ANIM_S);
    }

    private boolean calculFarfadet() {
        if (codeFonctionSecondaire == CODE_RESPONSABLE_FARFADETS || codeFonctionSecondaire == CODE_PARENTS_FARFADETS) return true;
        if (codeFonction == CODE_RESPONSABLE_FARFADETS || codeFonction == CODE_PARENTS_FARFADETS) return true;
        return false;
    }

    protected boolean calculCompa() {
        boolean ret = super.calculCompa();
        if (!ret) {
            if (codeFonctionSecondaire == CODE_ACCOMPAGNATEUR_COMPAS) ret = true;
            if (codeFonction == CODE_ACCOMPAGNATEUR_COMPAS) ret = true;
        }
        return ret;
    }

    public void addQualification(fr.sgdf.intranetapi.modeles.adherent.qualification.Qualification qualification) {
        String type = qualification.type;
        if (type.compareTo(DIRECTEUR_SF_CAFDSF) == 0) {
            Qualification o = new Qualification(type, qualification);
            pQualifications.put(qualification.estTitulaire ? DIR_T : DIR_S, o);
            pQualifications.put(DIR, o);
        }
        if (type.compareTo(ANIMATEUR_SF_CAFASF) == 0) {
            Qualification o = new Qualification(type, qualification);
            pQualifications.put(qualification.estTitulaire ? ANIM_T : ANIM_S, o);
            pQualifications.put(ANIM, o);
        }
    }

    public void addFormation(fr.sgdf.intranetapi.modeles.adherent.formation.Formation formation) {
        String tType = translations.getOrDefault(formation.type, formation.type);
        super.addFormation(formation);
        if (formationsFarfadets.contains(tType)) {
            pFormationsFarfadets.add(new Formation(tType, formation));
        }

        if (formationsCompas.contains(tType)) {
            pFormationsCompas.add(new Formation(tType, formation));
        }
    }

    public void addDiplome(fr.sgdf.intranetapi.modeles.adherent.diplome.Diplome diplome) {
        String tType = translations.getOrDefault(diplome.type, diplome.type);
        super.addDiplome(diplome);
        if (formationsFarfadets.contains(tType)) {
            pDiplomesFarfadets.add(new Diplome(tType, diplome));
        }

        if (formationsCompas.contains(tType)) {
            pDiplomesCompas.add(new Diplome(tType, diplome));
        }
    }

    public void complete() {
        super.complete();

        Formation approF = (Formation) pFormations.getT(APPRO);
        if (approF == null) {
            approF = (Formation) pFormations.getT(TECH);
            if (approF != null) {
                long fin = approF.ddate.getTime() + 0;
                long diffFindDec = (new Date().getTime() - fin) / 1000;
                diffFindDec /= (24 * 3600);
                if (diffFindDec < (365 + 365 + 365 + 183)) {
                    bafapotentiel = true;
                }
            }
            approF = (Formation) pFormations.get(BAFA_FORMATION_GENERALE);
            if (approF != null) {
                long fin = approF.ddate.getTime() + 0;
                long diffFindDec = (new Date().getTime() - fin) / 1000;
                diffFindDec /= (24 * 3600);
                if (diffFindDec < (365 + 365 + 365 + 183)) {
                    bafapotentiel = true;
                }
            }
        }

        Formation f = (Formation) pFormations.getT(APPRO);
        if (f != null) {
            appro = true;
        }
        f = (Formation) pFormations.getT(TECH);
        if (f != null) {
            tech = true;
        }
        f = (Formation) pFormations.getT(APF);
        if (f != null) {
            apf = true;
        }
        f = (Formation) pFormations.getT(APF_CHEFS_CHEFTAINES);
        if (f != null) {
            apf = true;
        }
    }

    public void transform(fr.sgdf.intranetapi.modeles.adherent.Adherent adherent) {
        super.transform(adherent);
        codeFonctionSecondaireComplet = codeSecondaire(adherent.fonctionsSecondaires);
        codeFonctionSecondaire = codeFonctionSecondaireComplet.isEmpty() ? 0 : Utils.extraitCodeFonction(codeFonctionSecondaireComplet);
        fonctionSecondaire = nomSecondaire(adherent.fonctionsSecondaires);

        codeFonctionSecondaire_classe = Utils.calculClasseFonction(codeFonctionSecondaire);

        if (adherent.intervenantJS != null) {
            fonctionJS = adherent.intervenantJS.fonctionJS;
            if (fonctionJS == null) fonctionJS = "";
            diplomeJS = adherent.intervenantJS.diplomeJS;
            if (diplomeJS == null) diplomeJS = "";
            diplomeDetailJS = adherent.intervenantJS.diplomeDetailJS;
            if (diplomeDetailJS == null) diplomeDetailJS = "";
            qualiteJS = adherent.intervenantJS.qualiteJS;
            if (adherent.intervenantJS.derniereModification != null) {
                this.derniereModification = convertToDateViaSqlTimestamp(adherent.intervenantJS.derniereModification);
            }
        } else {
            if (fonctionJS == null) fonctionJS = "";
            if (diplomeJS == null) diplomeJS = "";
            if (diplomeDetailJS == null) diplomeDetailJS = "";
        }

        Date dn = adherent.dateDeNaissance;
        Date debutCamp = Params.getDateDebutCamp();
        double diffCamp = ((debutCamp.getTime() - dn.getTime()) / 1000);
        diffCamp = diffCamp / (3600 * 365.25 * 24);

        boolean lage18ans1Juillet = false;
        boolean lageDirecteur = false;
        if (diffCamp > 0) {
            if (codeFonction == CODE_RESPONSABLE_FARFADETS || codeFonction == CODE_PARENTS_FARFADETS) lage18ans1Juillet = true;
            lage18ans1Juillet = diffCamp >= 17;
        }

        if (codeFonction == CODE_RESPONSABLE_FARFADETS || codeFonction == CODE_PARENTS_FARFADETS) {
            lageDirecteur = true;
        }
        lageDirecteur = (codeFonction < CODE_CHEFS_PIOK) ? diffCamp >= 19 : diffCamp >= 21;

        ageDirecteur = lageDirecteur;
        age18ans1Juillet = lage18ans1Juillet;

        compa = calculCompa();
        farfadet = calculFarfadet();
    }

    private String nomSecondaire(List<FonctionPrincipale> data) {
        if (data != null && data.size() > 0) return data.get(0).fonction.nom; else return null;
    }

    private String codeSecondaire(List<FonctionPrincipale> data) {
        if (data != null && data.size() > 0) return data.get(0).fonction.code; else return "";
    }

    private Qualification getQualif(String nom) {
        Object f = pQualifications.get(nom);
        if (f != null) {
            return (Qualification) f;
        }
        return new Qualification(null, null);
    }

    private Qualification getQualifNull(String nom) {
        return (Qualification) pQualifications.get(nom);
    }

    private Formation getFormationT(String nom) {
        Formation f = pFormations.getT(nom);
        if (f != null) {
            return (Formation) f;
        }
        return new Formation(null, null);
    }

    private Formation getFormation(String nom) {
        Formation f = pFormations.get(nom);
        if (f != null) {
            return (Formation) f;
        }
        return new Formation(null, null);
    }

    private Formation getFormationTNull(String string) {
        return (Formation) pFormations.getT(nom);
    }

    private Formation getFormationNull(String noms[]) {
        for (int i = 0; i < noms.length; i++) {
            Formation f = getFormationTNull(noms[i]);
            if (f != null) {
                return f;
            }
        }
        return null;
    }

    private Diplome getDiplome(String nom) {
        Diplome f = pDiplomes.get(nom);
        if (f != null) {
            return (Diplome) f;
        }
        return new Diplome(null, null);
    }

    private Diplome getDiplomeT(String nom) {
        Diplome f = pDiplomes.getT(nom);
        if (f != null) {
            return (Diplome) f;
        }
        return new Diplome(null, null);
    }

    public void majUnite(Unite uniteObj) {
        if (statusE != Statut.ADHERENT) return;

        boolean dirsf = getQualif(DIR).defini;
        boolean dirsfQualifie = getQualif(DIR).defini && getQualif(DIR).titulaire;
        boolean dirsfNonQualifie = getQualif(DIR).defini && !getQualif(DIR).titulaire;
        boolean animsfQualifie = getQualif(ANIM).defini && getQualif(ANIM).titulaire;
        boolean animsfNonQualifie = getQualif(ANIM).defini && !getQualif(ANIM).titulaire;

        boolean apf = getFormation("APF CHEFS - CHEFTAINES").defini;
        boolean apf_chefs = getFormation("APF CHEFS - CHEFTAINES").defini;
        boolean apf_rg = getFormation("APF RESPONSABLE DE GROUPE").defini;
        boolean apf_sp = getFormation("APF SECRETAIRE TRESORIER").defini;
        boolean apf_rldr = getFormation("APF RESPONSABLE LOCAL DEVELOPPEMENT ET RESEAUX").defini;
        boolean apf_aavsc = getFormation("APF AUMONIER ET AVSC").defini;

        boolean tech = getFormationT(TECH).defini;
        boolean appro = getFormationT(APPRO).defini;
        boolean appro_anim = getFormation("APPRO ANIMATION").defini;
        boolean appro_accueil = getFormation("APPRO ACCUEIL").defini;

        boolean module_appro_accueil_scoutisme = getFormation("MODULE APPRO ACCUEIL DE SCOUTISME").defini;
        boolean module_animateur_scoutisme_campisme = getFormation("MODULE ANIMATEUR DE SCOUTISME ET CAMPISME").defini;
        boolean module_appro_surveillant_baignade = getFormation("MODULE APPRO SURVEILLANT BAIGNADE").defini;

        boolean aqualif = false;
        boolean aqualifsf = false;

        boolean responsable_farfadets = getFormation("FORMATION DES RESPONSABLES FARFADETS").defini;
        if (responsable_farfadets) {
            uniteObj.responsablefarfadets++;
        }

        if (dirsf) {
            if (uniteObj.dirsfqnonq == 0) {
                uniteObj.dirsfqnonq++;
            }
        }
        if (dirsfQualifie) {
            if (uniteObj.dirsf == 0) {
                uniteObj.dirsf++;
                aqualif = true;
                aqualifsf = true;
            } else {
                animsfQualifie = true;
                aqualifsf = false;
            }
        }
        if (animsfQualifie && !aqualifsf && !compa) {
            uniteObj.animsf++;
            aqualif = true;
        }
        if ((animsfNonQualifie || dirsfNonQualifie) && (!animsfQualifie && !dirsfQualifie)) {
            uniteObj.stagiaires++;
            aqualif = true;
        }
        if (appro) {
            uniteObj.appro++;
            aqualif = true;
        }
        if (module_appro_accueil_scoutisme) {
            uniteObj.moduleapproaccueilscoutisme++;
        }
        if (module_animateur_scoutisme_campisme) {
            uniteObj.moduleanimateurscoutismecampisme++;
        }
        if (module_appro_surveillant_baignade) {
            uniteObj.moduleapprosurveillantbaignade++;
        }
        if (appro_accueil) {
            uniteObj.approaccueil++;
            aqualif = true;
        }
        if (appro_anim) {
            uniteObj.approanim++;
            aqualif = true;
        }
        if (tech) {
            uniteObj.tech++;
            aqualif = true;
        }
        if (apf || apf_chefs || apf_rg) {
            uniteObj.apf++;
            aqualif = true;
        }
        if (apf_sp) uniteObj.apf++;
        if (apf_rldr) uniteObj.apf++;
        if (apf_aavsc) uniteObj.apf++;

        if (!aqualif) uniteObj.autresCamp++;

        if (!compa && (animsfQualifie || dirsfQualifie)) {
            uniteObj.qualifs++;
        }

        if (getFormation("CHAM").defini) uniteObj.cham++;
        if (getFormation("STAF").defini) uniteObj.staf++;

        if (getDiplomeT("PSC1").defini) uniteObj.psc1++;
        if (getDiplome("AFPS").defini) uniteObj.psc1++;
        if (getDiplome("BAFA").defini) uniteObj.bafa++;
        if (getDiplome("BAFD").defini) uniteObj.bafa++;
        if (getDiplome("2 Buchettes").defini) uniteObj.buchettes++;
        if (getDiplome("3 Buchettes").defini) uniteObj.buchettes++;
        if (getDiplome("4 Buchettes").defini) uniteObj.buchettes++;

        uniteObj.ajouter(this.getJeune(), this.getChef());
    }

    public void genererAlertes(Collection<Alerte> alertes) {
        //		String dateFinMission = this.get(colonnes_.getDateFinMissionId());
        //		if (dateFinMission.isEmpty()) {
        //			ajouterAlerte(adherent, Alerte.Severite.MOYENNE, Alerte.ALERTE_TYPE_INSCRIPTION, "L'adhérent n'a pas demandé sa nomination");
        //		}

        Qualification qanimsf = getQualifNull(ANIM);
        Qualification qdirfs = getQualifNull(DIR);
        if (qdirfs != null) {
            Object fin = qdirfs.date;
            if (fin != null && fin instanceof String) {
                if (((String) fin).compareTo(PAS_DE_DATE) == 0) {
                    ajouterAlerte(alertes, this, Alerte.Severite.MOYENNE, Alerte.ALERTE_TYPE_QUALIFICATION, "DirSF permanente");
                }
            }
            if (qdirfs.dejaExpire) {
                if (qdirfs.expiration < 365 * 2) ajouterAlerte(
                    alertes,
                    this,
                    Alerte.Severite.HAUTE,
                    Alerte.ALERTE_TYPE_QUALIFICATION,
                    "DirSF expirée"
                );
            } else if (qdirfs.expireAvantCamp) {
                ajouterAlerte(alertes, this, Alerte.Severite.MOYENNE, Alerte.ALERTE_TYPE_QUALIFICATION, "DirSF expiré avant le camp");
            }
        }
        if (qanimsf != null && !qanimsf.titulaire) {
            Object fin = qanimsf.date;
            if (fin != null && fin instanceof String) {
                if (((String) fin).compareTo(PAS_DE_DATE) == 0) {
                    ajouterAlerte(alertes, this, Alerte.Severite.HAUTE, Alerte.ALERTE_TYPE_QUALIFICATION, "AnimSF stagiaire permanent");
                }
            }
        }

        Formation formationRG = getFormation("accueil_scoutisme_rg");
        if (formationRG.defini == true && qdirfs == null) {
            ajouterAlerte(
                alertes,
                this,
                Alerte.Severite.MOYENNE,
                Alerte.ALERTE_TYPE_QUALIFICATION,
                "RG avec formation mais non qualifié dirSF"
            );
        }

        if (age18ans1Juillet == false && this.compa == false) {
            ajouterAlerte(alertes, this, Alerte.Severite.HAUTE, Alerte.ALERTE_TYPE_AGE, "Pas 18 ans au 1er juillet prochain");
        }

        if (this.getAgeokcampb() == false && this.compa == false && qdirfs != null && qdirfs.titulaire) {
            if (codeFonction < CODE_CHEFS_PIOK) {
                ajouterAlerte(
                    alertes,
                    this,
                    Alerte.Severite.HAUTE,
                    Alerte.ALERTE_TYPE_AGE,
                    "Pas 19 ans au 1er juillet prochain pour être directeur"
                );
            } else {
                ajouterAlerte(
                    alertes,
                    this,
                    Alerte.Severite.HAUTE,
                    Alerte.ALERTE_TYPE_AGE,
                    "Pas 21 ans au 1er juillet prochain pour être directeur"
                );
            }
        }

        Formation formationTech = getFormation(TECH);
        if (formationTech.defini == true && qanimsf == null) {
            ajouterAlerte(alertes, this, Alerte.Severite.MOYENNE, Alerte.ALERTE_TYPE_QUALIFICATION, "Tech non qualifié animSF");
        }

        Formation formationApf = getFormationNull(new String[] { "apf", "apf_chefs" });
        if (formationApf != null && formationTech.defini == false) {
            Date date_aujourdhui = Date.from(Instant.now());
            Date datef = formationApf.ddate;
            long delta = ((date_aujourdhui.getTime() - datef.getTime()) / 1000) / 86400;
            if (delta <= 2 && qanimsf == null) {
                ajouterAlerte(alertes, this, Alerte.Severite.MOYENNE, Alerte.ALERTE_TYPE_QUALIFICATION, "Anim SF stagiaire potentiel");
            }
        }
    }

    public boolean getChef() {
        return codeFonction == CODE_IMPEESA || codeFonction >= CODE_RESPONSABLES ? true : false;
    }

    public boolean getJeune() {
        return !getChef();
    }

    public boolean getCompa() {
        if (codeFonctionSecondaire == CODE_COMPAS_T1T2 || codeFonctionSecondaire == CODE_COMPAS_T3) return true;
        return (codeFonction == CODE_COMPAS_T1T2 || codeFonction == CODE_COMPAS_T3) ? true : false;
    }

    private boolean getAgeokcampb() {
        if (ageCamp > 0) {
            if (codeFonction == CODE_RESPONSABLE_FARFADETS || codeFonction == CODE_PARENTS_FARFADETS) {
                return true;
            }
            if (codeFonction < CODE_CHEFS_PIOK) {
                if (ageCamp < 19) return false; else return true;
            } else {
                if (ageCamp < 21) return false; else return true;
            }
        }
        return true;
    }
}
