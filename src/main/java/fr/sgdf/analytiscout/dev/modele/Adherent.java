package fr.sgdf.analytiscout.dev.modele;

import fr.sgdf.analytiscout.dev.DiplomeSet;
import fr.sgdf.analytiscout.dev.FormationSet;
import fr.sgdf.analytiscout.dev.Params;
import fr.sgdf.analytiscout.dev.Utils;
import fr.sgdf.analytiscout.dev.anonymiser.Anon;
import fr.sgdf.analytiscout.dev.anonymiser.Anonymizer;
import fr.sgdf.intranetapi.modeles.adherent.Branche;
import fr.sgdf.intranetapi.utils.StructureUtils;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Adherent {

    protected static final Map<String, String> translations = new TreeMap<String, String>();

    static {
        translations.put("PSC1 Prevention et Secours Civiques de niveau 1", "PSC1");
        translations.put("PATRON D'EMBARCATION", "PE");
        translations.put("FORMATION INITIALE DES RESPONSABLES DE GROUPE", "FIRG");
        translations.put("FORMATION GENERALE RESPONSABLE DE GROUPE", "FGRG");
        translations.put("FORMATION INITIALE DE FORMATEURS", "FIF");
        translations.put("FORMATION CONTINUE DE FORMATEURS", "FCF");
        translations.put("MODULE APPRO RENCONTRES INTERNATIONALES", "MARI");
        translations.put("FORMATION COMPAGNON 1ER TEMPS", "FC1");
        translations.put("FORMATION COMPAGNON 2EME TEMPS", "FC2");
        translations.put("FORMATION DES ACCOMPAGNATEURS COMPAGNONS", "FAC");
        translations.put("AIDE A LA PRISE DE FONCTION", "APF");
        translations.put("APF CHEFS - CHEFTAINES", "APF");
        translations.put("APF RESPONSABLE DE GROUPE", "APF");
        translations.put("APPRO ACCUEIL", "APPRO");
        translations.put("APPRO ACCUEIL DE SCOUTISME", "APPRO");
        translations.put("MODULE APPRO ACCUEIL DE SCOUTISME", "APPRO");
        translations.put("MODULE APPRO ANIMATION", "APPRO");
        translations.put("APPRO ANIMATION", "APPRO");
        translations.put("APF RESPONSABLE DE GROUPE", "APF RG");
        translations.put("FORMATION INITIALE TRESORIER-SECRETAIRE", "FITS");

        translations.put("PATRON D'EMBARCATION", "PE");
        translations.put("Patron d'embarcation", "PE");
        translations.put("Chef de quart", "CQ");
        translations.put("Chef de Flotille", "CF");
    }

    public static final int CODE_COMPAS_T1T2 = 140;
    public static final int CODE_COMPAS_T3 = 141;
    public static final int CODE_LJ = 110;
    public static final int CODE_CHEFS_LJ = 210;
    public static final int CODE_SG = 120;
    public static final int CODE_CHEFS_SG = 220;
    public static final int CODE_PIOK = 130;
    public static final int CODE_FARFADETS = 170;
    public static final int CODE_CHEFS_PIOK = 230;

    private static final String AFPS = "AFPS";
    private static final String PSC1 = "PSC1";

    public long codeAdherent;
    public String prenom;
    public String nom;
    public int codeGroupe;
    public int codeStructure;
    public String nomStructure;
    public int codeFonction;
    public String codeFonctionComplet;
    public int codeFonctionSecondaire;
    public String branche_abreviation;
    public Branche branche;
    public String branche_classe;
    public boolean marin;
    private Date dateDeNaissance;

    public boolean droitImage;
    public boolean droitInformations;

    public String status;
    public fr.sgdf.intranetapi.modeles.adherent.Statut statusE;

    public String fonction;
    public String codeFonction_classe;

    public boolean psc1;

    public List<Fonction> fonctions = new ArrayList<Fonction>();

    protected DiplomeSet pDiplomes = new DiplomeSet();
    protected FormationSet pFormations = new FormationSet();

    public float age;
    public double ageCamp = -1;
    public double ageFinDec = -1;

    public void transform(fr.sgdf.intranetapi.modeles.adherent.Adherent adherent) {
        codeAdherent = adherent.codeAdherent;
        prenom = adherent.prenom;
        nom = adherent.nom;
        nomStructure = adherent.fonctionPrincipale.structure.nom;
        codeStructure = adherent.fonctionPrincipale.structure.code;
        codeFonction = Utils.extraitCodeFonction(adherent.fonctionPrincipale.fonction.code);
        codeFonctionComplet = adherent.fonctionPrincipale.fonction.code;
        codeGroupe = calculCodeGroupe(adherent.fonctionPrincipale.structure.code);
        branche = StructureUtils.getBrancheFromCode(adherent.fonctionPrincipale.structure.code);
        branche_abreviation = Utils.calculAbreviationBranche(branche);
        branche_classe = Utils.calculClasseBranche(branche);
        marin = adherent.fonctionPrincipale.fonction.code.endsWith("M");
        dateDeNaissance = adherent.dateDeNaissance;
        droitImage = adherent.droitImage;
        droitInformations = adherent.droitsInformations;
        status = adherent.statutE.name();
        statusE = adherent.statutE;

        fonction = adherent.fonctionPrincipale.fonction.nom;
        codeFonction_classe = Utils.calculClasseFonction(codeFonction);

        fonctions.add(
            new Fonction(
                adherent.fonctionPrincipale.fonction.nom,
                adherent.fonctionPrincipale.structure.nom,
                adherent.fonctionPrincipale.fonction.code,
                adherent.fonctionPrincipale.structure.code
            )
        );
        if (adherent.fonctionsSecondaires != null) {
            adherent.fonctionsSecondaires.forEach(fonction ->
                fonctions.add(new Fonction(fonction.fonction.nom, fonction.structure.nom, fonction.fonction.code, fonction.structure.code))
            );
        }

        Date dn = adherent.dateDeNaissance;
        long aj = Instant.now().toEpochMilli();
        float diff = ((aj - dn.getTime()) / 1000);
        diff = diff / (3600 * 365 * 24);
        age = diff;

        Date debutFindDec = Params.getDateLimiteJeune();
        double diffFindDec = ((debutFindDec.getTime() - dn.getTime()) / 1000);
        diffFindDec = diffFindDec / (3600 * 365.25 * 24);
        ageFinDec = diffFindDec;

        Date debutCamp = Params.getDateDebutCamp();
        double diffCamp = ((debutCamp.getTime() - dn.getTime()) / 1000);
        diffCamp = diffCamp / (3600 * 365.25 * 24);
        ageCamp = diffCamp;
    }

    public void complete() {
        Diplome d = (Diplome) pDiplomes.getT(PSC1);
        if (d != null) {
            psc1 = true;
        }
        d = (Diplome) pDiplomes.getT(AFPS);
        if (d != null) {
            psc1 = true;
        }
    }

    public void addFormation(fr.sgdf.intranetapi.modeles.adherent.formation.Formation formation) {
        String tType = translations.getOrDefault(formation.type, formation.type);
        pFormations.add(new Formation(tType, formation));
    }

    public void addDiplome(fr.sgdf.intranetapi.modeles.adherent.diplome.Diplome diplome) {
        String tType = translations.getOrDefault(diplome.type, diplome.type);
        pDiplomes.add(new Diplome(tType, diplome));
    }

    protected List<Diplome> getDiplomesPriv(DiplomeSet pDiplomes) {
        List<Diplome> d = new ArrayList<Diplome>();
        pDiplomes.forEach(value -> d.add(value));
        Collections.sort(d);
        return d;
    }

    public Formation getFormationPe() {
        return pFormations.getT("PE");
    }

    public Diplome getDiplomePe() {
        return pDiplomes.getT("PE");
    }

    public Collection<Diplome> getDiplomes() {
        return getDiplomesPriv(pDiplomes);
    }

    public void anonymiser() {
        Anonymizer anonymiser = Anon.getAnonymizer();
        if (anonymiser != null) {
            nom = anonymiser.prochainNom();
            prenom = anonymiser.prochainPrenom();
        }
    }

    protected Date dateAge(int age) {
        double d = dateDeNaissance.getTime() + age * 3600 * 365.25 * 24 * 1000;
        Date dd = new Date((long) Math.floor(d));
        return dd;
    }

    static public int calculCodeGroupe(int codeStructure) {
        int codeGroupe = codeStructure / 100;
        return codeGroupe * 100;
    }

    protected void ajouterAlerte(Collection<Alerte> alertes, Adherent adherent, Alerte.Severite severite, String type, String message) {
        alertes.add(new Alerte(adherent, severite, type, message));
    }

    protected boolean calculCompa() {
        if (codeFonctionSecondaire == Adherent.CODE_COMPAS_T1T2 || codeFonctionSecondaire == Adherent.CODE_COMPAS_T3) return true;
        return (codeFonction == Adherent.CODE_COMPAS_T1T2 || codeFonction == Adherent.CODE_COMPAS_T3) ? true : false;
    }

    public int getCodeFonction() {
        return this.codeFonction;
    }
}
