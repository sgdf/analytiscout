package fr.sgdf.analytiscout.dev.modele;

public class Fonction {

	public String nom;
	public String nomStructure;
	public String codeFonction;
	public int codeStructure;
		
	public Fonction(String nom, String nomStructure, String codeFonction, int codeStructure) {
		this.nom = nom;
		this.nomStructure = nomStructure;
		this.codeFonction = codeFonction;
		this.codeStructure = codeStructure;
	}
}
