package fr.sgdf.analytiscout.dev.modele;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import fr.sgdf.intranetapi.modeles.structure.Structure;

public class Structures {
	public List<Structure> structures;
	
	public void check() throws ResponseStatusException {
		Boolean isLocal = null;
		Integer groupeCode = null;
		for (Structure structure : structures) {
			if (isLocal == null) {
				isLocal = structure.isEchelonLocale();
			} else {
				// Pas de mélange groupe / territoire
				if (isLocal != structure.isEchelonLocale()) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Impossible de mixer groupes & territoire");
				}
			}
			if (isLocal == true) {
				if (groupeCode == null) {
					groupeCode = (structure.getCodeStructure() / 100) * 100;
				} else {
					// Pas de mélange des groupes
					if (groupeCode !=  ((structure.getCodeStructure() / 100) * 100)) {
						throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Impossible de mixer des groupes");
					}
				}
			}
		}
	}
}
