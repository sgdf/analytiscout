package fr.sgdf.analytiscout.dev.modele;

public class QuotaAnnee extends Quota {

    QuotaAnnee(Unite unite) {
        super(unite);
    }

    public void finalise() {
        super.finalise();
        quotaDir = quotaDir_effectif - quotaDir_theorique;
    }
}
