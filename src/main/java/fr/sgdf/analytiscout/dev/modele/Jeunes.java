package fr.sgdf.analytiscout.dev.modele;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import fr.sgdf.intranetapi.modeles.adherent.Adherent;

public class Jeunes {
	private Map<Long, Jeune> adherentsPriv = new HashMap<Long, Jeune>();
	public Collection<Alerte> alertes = new ArrayList<Alerte>();

	public void complete() {
		adherentsPriv.forEach((ca,r) -> 
		{
			r.complete();
		});
	}

	public void calcul(boolean calculAge) {
		adherentsPriv.forEach((j,r) -> {
			r.genererAlertes(calculAge, alertes);
		});
	}

	public void transform(List<Adherent> jeunes) {
		jeunes.forEach(r -> {
			Jeune rt = new Jeune();
			rt.transform(r);
			adherentsPriv.put(rt.codeAdherent, rt);
		});
	}
	
	public Jeune getJeune(Long codeAdherent) {
		return adherentsPriv.get(codeAdherent);
	}
	
	public Collection<Jeune> getAdherents() {
		Collection<Jeune> adherentsAr = adherentsPriv.values().stream().sorted(Comparator.comparing(Jeune::getTriKey)).collect(Collectors.toList());
		return adherentsAr;
	}

	public void trier() {
		alertes = alertes.stream().sorted(Comparator.comparingInt(Alerte::getSeveriteInt).reversed()).collect(Collectors.toList());
	}
}
