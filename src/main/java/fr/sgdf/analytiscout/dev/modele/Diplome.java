package fr.sgdf.analytiscout.dev.modele;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import fr.sgdf.analytiscout.dev.Utils;

public class Diplome implements Comparable<Diplome> {
	
	static private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY");
	
	public Diplome(String tType, fr.sgdf.intranetapi.modeles.adherent.diplome.Diplome diplome) {
		if (diplome != null && diplome.dateObtention != null) {
			this.ddate = convertToDateViaSqlTimestamp(diplome.dateObtention);
			this.date = diplome.dateObtention.format(formatter);
			defini = true;
			this.type = diplome.type;
			this.tType = tType;
		}
		if (diplome != null) this.iSgdfType = Utils.iconeSgdfDiplome(diplome.type);
		if (diplome != null) this.iSgdfClasseLarge = Utils.iconeSgdfDiplomeClasseLarge(diplome.type);
		if (diplome != null) this.iFaType = Utils.iconeFaDiplome(diplome.type);
	}
	
	private Date convertToDateViaSqlTimestamp(LocalDateTime dateToConvert) {
	    return java.sql.Timestamp.valueOf(dateToConvert);
	}

	public String tType;
	public String type;
	public Date ddate;
	public String date;
	public boolean defini;
	public String iSgdfType;
	public boolean iSgdfClasseLarge;
	public String iFaType;

	@Override
	public int compareTo(Diplome o) {
		return this.ddate.compareTo(o.ddate);
	}
}
