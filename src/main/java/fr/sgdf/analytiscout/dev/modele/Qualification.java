package fr.sgdf.analytiscout.dev.modele;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import fr.sgdf.analytiscout.dev.Params;

public class Qualification implements Comparable<Qualification> {
	
	static private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY");
	static private DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	
	public Qualification(String tType, fr.sgdf.intranetapi.modeles.adherent.qualification.Qualification qualification) {
		if (qualification != null) {
			defini = true;
			this.type = qualification.type;
			this.tType = tType;
			this.titulaire = qualification.estTitulaire;
			this.titulaire_value = qualification.estTitulaire ? "Oui" : "Non";
			if (qualification.dateFin != null) {
				this.date = qualification.dateFin.format(formatter);
				this.ddate = convertToDateViaSqlTimestamp(qualification.dateFin);
				try {
					Date date = df.parse(this.date);
					Date date_aujourdhui = Date.from(Instant.now());
					Date debutCamp = Params.getDateDebutCamp();
					if (date.before(date_aujourdhui))
					{
						this.dejaExpire = true;
						this.etat_classe="expire";
						
						expiration = ((date_aujourdhui.getTime() - date.getTime())/1000)/86400;
					}
					else
					if (date.before(debutCamp))
					{
						this.etat_classe="vaexpirer";
						this.expireAvantCamp = true;
					}
					else {
						this.etat_classe = "ok";
					}
				} catch (ParseException e) {
				}
			}
			else {
				this.date = this.titulaire ? "Permanent" : "Temporaire";
				this.etat_classe = "ok";
			}
		}
	}
	
	private Date convertToDateViaSqlTimestamp(LocalDateTime dateToConvert) {
	    return java.sql.Timestamp.valueOf(dateToConvert);
	}

	@Override
	public int compareTo(Qualification o) {
		if (this.ddate != null && o.ddate != null)
			return this.ddate.compareTo(o.ddate);
		return 0;
	}

	public String tType;
	public String type;
	public boolean titulaire;
	public String titulaire_value;
	public String date;
	public Date ddate;
	public String etat_classe;
	public boolean expireAvantCamp;
	public boolean dejaExpire;
	public long expiration;
	public boolean defini;
}
