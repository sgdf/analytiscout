package fr.sgdf.analytiscout.dev.modele;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import fr.sgdf.analytiscout.dev.Utils;
import fr.sgdf.intranetapi.modeles.adherent.formation.Formation.Role;

public class Formation implements Comparable<Formation> {
	
	static private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY");
	
	public Formation(String tType, fr.sgdf.intranetapi.modeles.adherent.formation.Formation formation) {
		if (formation != null && formation.dateFin != null) {
			this.ddate = convertToDateViaSqlTimestamp(formation.dateFin);
			this.date = formation.dateFin.format(formatter);
			this.defini = true;
			this.type = formation.type;
			this.tType = tType;
			this.formateur = ((formation.role == Role.Formateur) || (formation.role == Role.Directeur));
			this.divers = Utils.isFormationDivers(tType);
			
			if (formation != null) this.iSgdfType = Utils.iconeSgdfFormation(formation.type);
			if (formation != null) this.iFaType = Utils.iconeFaFormation(formation.type);
		}
	}
	
	private Date convertToDateViaSqlTimestamp(LocalDateTime dateToConvert) {
	    return java.sql.Timestamp.valueOf(dateToConvert);
	}

	public String tType;
	public String type;
	public Date ddate;
	public String date;
	public boolean formateur;
	public boolean defini;
	public boolean divers;
	public String iSgdfType;
	public String iFaType;

	@Override
	public int compareTo(Formation o) {
		return this.ddate.compareTo(o.ddate);
	}
}
