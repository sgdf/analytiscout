package fr.sgdf.analytiscout.dev.modele;

import fr.sgdf.analytiscout.dev.Utils;
import fr.sgdf.intranetapi.modeles.adherent.Branche;
import fr.sgdf.intranetapi.utils.StructureUtils;

public class Unite {
	
	public Unite(String nom, int codeStructure, int fonction) {
		this.nom = nom;
		this.codeFonction = fonction;
		this.codeStructure = codeStructure;
		this.groupe = new Groupe(nom, codeStructure/100);
		this.branche = StructureUtils.getBrancheFromCode(this.codeStructure);
		this.branche_classe = Utils.calculClasseBranche(this.branche);
		this.branche_abreviation = Utils.calculAbreviationBranche(this.branche);
	}
	
	public String nomStructure;
	public String nom;
	public Branche branche;
	public String branche_classe;
	public String branche_abreviation;
	public int codeFonction=999;
	public int codeStructure;
	public Groupe groupe;
	public int montees;
	
	public int jeunes;
	public int chefs;
	public float ratio;
	
	public int apf;
	public int tech;
	public int appro;
	public int approaccueil;
	public int approanim;
	public int psc1;
	public int bafa;
	public int bafd;
	public int animsf;
	public int dirsfqnonq;
	public int dirsf;
	public int cham;
	public int staf;
	public int buchettes;
	public int moduleapproaccueilscoutisme;
	public int moduleanimateurscoutismecampisme;
	public int moduleapprosurveillantbaignade;
	public int responsablefarfadets;
	
	public int stagiaires;
	public int qualifs;
	public int autresCamp;
	
	public void calcul() {
		if (chefs > 0 && jeunes > 0) {
			ratio = chefs / jeunes;
		}
	}
	
	public void ajouter(boolean jeune, boolean chef)
	{
		jeunes+=jeune ? 1 : 0;
		chefs+=chef ? 1 : 0;
	}
	
	public int getCodeFonction() {
		return this.codeFonction;
	}
	
	public int getCodeStructure() {
		return this.codeStructure;
	}
}
