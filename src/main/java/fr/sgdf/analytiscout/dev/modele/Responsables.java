package fr.sgdf.analytiscout.dev.modele;

import fr.sgdf.analytiscout.dev.Utils;
import fr.sgdf.analytiscout.dev.anonymiser.Anon;
import fr.sgdf.analytiscout.dev.anonymiser.Anonymizer;
import fr.sgdf.intranetapi.modeles.adherent.Branche;
import fr.sgdf.intranetapi.modeles.adherent.Statut;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Responsables {

    private Map<Long, Responsable> adherentsPriv = new HashMap<Long, Responsable>();
    public Collection<Alerte> alertes = new ArrayList<Alerte>();
    private Map<String, Unite> unites = new HashMap<String, Unite>();
    public Collection<QuotaUnite> quotaunite = new ArrayList<QuotaUnite>();
    public Collection<QuotaAnnee> quotaannee = new ArrayList<QuotaAnnee>();

    public void complete() {
        adherentsPriv.forEach((ca, r) -> {
            r.complete();
            r.nettoyage();
        });
    }

    public void calculFinal() {
        this.unites.forEach((k, v) -> {
                v.calcul();
            });

        Map<Integer, QuotaAnnee> mapQuotasAnnees = new TreeMap<Integer, QuotaAnnee>();
        this.unites.forEach((key, unite) -> {
                QuotaUnite qunite = new QuotaUnite(unite);
                quotaunite.add(qunite);
                qunite.complete(unite);
                qunite.finalise();

                QuotaAnnee qannee = mapQuotasAnnees.computeIfAbsent(unite.groupe.getCode(), k -> new QuotaAnnee(unite));
                qannee.complete(unite);
            });
        mapQuotasAnnees.forEach((k, v) -> {
            v.finalise();
        });
        quotaannee = mapQuotasAnnees.values();
    }

    public Responsable getResponsable(Long codeAdherent) {
        return adherentsPriv.get(codeAdherent);
    }

    public void calcul(List<fr.sgdf.intranetapi.modeles.adherent.Adherent> sJeunes) {
        adherentsPriv.forEach((c, r) -> {
            r.genererAlertes(alertes);
        });

        adherentsPriv.forEach((c, r) -> {
        	if (r.statusE == Statut.ADHERENT) {
	            String unite = r.nomStructure;
	            
	            // Fonction principale
	            Unite uniteObj = this.unites.computeIfAbsent(unite, k -> new Unite(unite, r.codeStructure, r.codeFonction));
	            uniteObj.codeFonction = r.codeFonction;
	            if (r.getChef()) {
	                r.majUnite(uniteObj);
	            }
	            
	            // Fonctions secondaires
	            r.fonctions.forEach(f -> {
	            	if (r.codeGroupe == Adherent.calculCodeGroupe(f.codeStructure)) {
	            		Unite uniteObjFs = this.unites.computeIfAbsent(f.nomStructure, k -> new Unite(f.nomStructure, f.codeStructure, Utils.extraitCodeFonction(f.codeFonction)));
	            		uniteObjFs.codeFonction = r.codeFonction;
	            	}
	            });
        	}
        });

        sJeunes.forEach(jeune -> {
            Unite uniteObj = this.unites.get(jeune.fonctionPrincipale.structure.nom);
            if (uniteObj != null && jeune.statutE == Statut.ADHERENT) {
                uniteObj.jeunes++;
            }
        });
    }

    public void transform(List<fr.sgdf.intranetapi.modeles.adherent.Adherent> responsables) {
        responsables.forEach(r -> {
            Responsable rt = new Responsable();
            rt.transform(r);
            adherentsPriv.put(rt.codeAdherent, rt);
        });
    }

    public void trier() {
        alertes = alertes.stream().sorted(Comparator.comparing(Alerte::getTriKey)).collect(Collectors.toList());
        quotaannee = quotaannee.stream().sorted(Comparator.comparingInt(QuotaAnnee::getCodestructure)).collect(Collectors.toList());
        quotaunite = quotaunite.stream().sorted(Comparator.comparingInt(QuotaUnite::getCodestructure)).collect(Collectors.toList());
    }

    public Collection<Responsable> getAdherents() {
        Collection<Responsable> adherentsAr = adherentsPriv
            .values()
            .stream()
            .sorted(Comparator.comparing(Responsable::getTriKey))
            .collect(Collectors.toList());
        return adherentsAr;
    }

    public Collection<Unite> getUnites() {
        Collection<Unite> unites =
            this.unites.values().stream().sorted(Comparator.comparingInt(Unite::getCodeStructure)).collect(Collectors.toList());
        return unites;
    }

    public void anonymiser() {
        Anonymizer anonymiser = Anon.getAnonymizer();
        if (anonymiser != null) {
            Map<String, String> tableDeTraductionNoms = new TreeMap<String, String>();
            Map<String, String> tableDeTraductionCode = new TreeMap<String, String>();

            Map<String, List<String>> unites = new TreeMap<String, List<String>>();
            adherentsPriv.forEach((ca, adherent) -> {
                String c = adherent.branche + "-" + adherent.codeGroupe;
                List<String> us = unites.get(c);
                if (us == null) {
                    us = new ArrayList<String>();
                    unites.put(c, us);
                }
                int index = us.indexOf("" + adherent.codeStructure);
                if (index == -1) us.add("" + adherent.codeStructure);
            });

            AtomicInteger groupeId = new AtomicInteger();
            adherentsPriv.forEach((ca, adherent) -> {
                String groupe = "" + adherent.codeGroupe;
                String unite = adherent.nomStructure;

                if (tableDeTraductionNoms.containsKey(unite) == false) {
                    if (unite.startsWith("TERRITOIRE ")) {
                        unite = "TERRITOIRE " + "UNIVERS";
                        tableDeTraductionNoms.put(adherent.nomStructure, unite);
                    } else if (unite.startsWith("GROUPE ")) {
                        unite = "GROUPE A" + groupeId.incrementAndGet();
                        tableDeTraductionNoms.put(adherent.nomStructure, unite);
                        tableDeTraductionCode.put(groupe, unite);
                    } else {
                        unite = "STRUCTURE AUTRE";
                        tableDeTraductionNoms.put(adherent.nomStructure, unite);
                        tableDeTraductionCode.put(groupe, unite);
                    }
                }
            });

            adherentsPriv.forEach((ca, adherent) -> {
                String unite = adherent.nomStructure;
                String c = adherent.branche + "-" + adherent.codeGroupe;

                if (unite.startsWith("RÉSEAU IMPEESA")) {
                    unite = "RÉSEAU IMPEESA " + tableDeTraductionCode.get("" + adherent.codeGroupe);
                    tableDeTraductionNoms.put(adherent.nomStructure, unite);
                } else {
                    Branche branche = adherent.branche;
                    List<String> us = unites.get(c);
                    if (branche.compareTo(Branche.FARFADET) == 0) {
                        if (us.size() == 1) {
                            tableDeTraductionNoms.put(
                                adherent.nomStructure,
                                "FARFADETS " + tableDeTraductionCode.get("" + adherent.codeGroupe)
                            );
                        } else {
                            int index = us.indexOf("" + adherent.codeStructure);
                            tableDeTraductionNoms.put(
                                adherent.nomStructure,
                                "FARFADETS " + tableDeTraductionCode.get("" + adherent.codeGroupe) + " UNITE " + (index + 1)
                            );
                        }
                    }
                    if (branche.compareTo(Branche.LOUVETEAU_JEANNETTE) == 0) {
                        if (us.size() == 1) {
                            tableDeTraductionNoms.put(
                                adherent.nomStructure,
                                "LOUVETEAUX JEANNETTES " + tableDeTraductionCode.get("" + adherent.codeGroupe)
                            );
                        } else {
                            int index = us.indexOf("" + adherent.codeStructure);
                            tableDeTraductionNoms.put(
                                adherent.nomStructure,
                                "LOUVETEAUX JEANNETTES " + tableDeTraductionCode.get("" + adherent.codeGroupe) + " UNITE " + (index + 1)
                            );
                        }
                    }
                    if (branche.compareTo(Branche.SCOUT_GUIDE) == 0) {
                        if (us.size() == 1) {
                            tableDeTraductionNoms.put(
                                adherent.nomStructure,
                                "SCOUTS GUIDES " + tableDeTraductionCode.get("" + adherent.codeGroupe)
                            );
                        } else {
                            int index = us.indexOf("" + adherent.codeStructure);
                            tableDeTraductionNoms.put(
                                adherent.nomStructure,
                                "SCOUTS GUIDES " + tableDeTraductionCode.get("" + adherent.codeGroupe) + " UNITE " + (index + 1)
                            );
                        }
                    }
                    if (branche.compareTo(Branche.PIONNIER_CARAVELLE) == 0) {
                        if (us.size() == 1) {
                            tableDeTraductionNoms.put(
                                adherent.nomStructure,
                                "PIONNNERS CARAVELLES " + tableDeTraductionCode.get("" + adherent.codeGroupe)
                            );
                        } else {
                            int index = us.indexOf("" + adherent.codeStructure);
                            tableDeTraductionNoms.put(
                                adherent.nomStructure,
                                "PIONNNERS CARAVELLES " + tableDeTraductionCode.get("" + adherent.codeGroupe) + " UNITE " + (index + 1)
                            );
                        }
                    }
                    if (branche.compareTo(Branche.COMPAGNON) == 0) {
                        if (us.size() == 1) {
                            tableDeTraductionNoms.put(
                                adherent.nomStructure,
                                "COMPAGNONS " + tableDeTraductionCode.get("" + adherent.codeGroupe)
                            );
                        } else {
                            int index = us.indexOf("" + adherent.codeStructure);
                            tableDeTraductionNoms.put(
                                adherent.nomStructure,
                                "COMPAGNONS " + tableDeTraductionCode.get("" + adherent.codeGroupe) + " UNITE " + (index + 1)
                            );
                        }
                    }
                }
            });

            AtomicInteger ai = new AtomicInteger(100000000);
            adherentsPriv.forEach((ca, entry) -> {
                int code = ai.incrementAndGet();
                entry.codeAdherent = (long) code;
                entry.nom = anonymiser.prochainNom();
                entry.prenom = anonymiser.prochainPrenom();

                // Nom de la structure
                anonymiserStructure(entry, tableDeTraductionNoms, tableDeTraductionCode);

                entry.codeStructure += 100000000;
            });
        }
    }

    private void anonymiserStructure(
        Responsable entry,
        Map<String, String> tableDeTraductionNoms,
        Map<String, String> tableDeTraductionCode
    ) {
        entry.nomStructure = tableDeTraductionNoms.getOrDefault(entry.nomStructure, entry.nomStructure);
    }
}
