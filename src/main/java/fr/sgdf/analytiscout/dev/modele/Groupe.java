package fr.sgdf.analytiscout.dev.modele;

public class Groupe implements Comparable<Groupe> {
	public String nom;
	public int code;
	
	public Groupe(String nom, int codeStructure)
	{
		this.nom = nom;
		this.code = codeStructure;
		this.code = code * 100;
	}

	@Override
	public int compareTo(Groupe o) {
		return Integer.compare(code, o.code);
	}
	
	public int getCode() {
		return code;
	}
}
