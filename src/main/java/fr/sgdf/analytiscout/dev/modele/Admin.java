package fr.sgdf.analytiscout.dev.modele;

import java.util.Map;

import fr.sgdf.intranetapi.ApiSessionDetails;
import fr.sgdf.intranetapi.modeles.utils.ApiSessionPoolStats;

public class Admin {
	public Map<String, ApiSessionDetails> sessions;
	public ApiSessionPoolStats poolDetails;
}
