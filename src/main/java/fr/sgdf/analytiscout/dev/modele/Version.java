package fr.sgdf.analytiscout.dev.modele;

import com.jcabi.manifests.Manifests;

public class Version {
	private boolean complete_;
	private int major_;
	private int minor_;
	private int subminor_;
	private String mode_;
	private String buildDate_;
	private boolean active_;
	private String workMessage_;
	
	public int getMajor() { return major_; };
	public int getMinor() { return minor_; };
	public int getSubMinor() { return subminor_; };
	public String getMode() { return mode_; }
	public String getBuildDate() { return buildDate_; }
	public boolean getWork() { return active_; }
	public String getWorkMessage() { return workMessage_; }
	
	@Override
	public String toString()
	{
		if (complete_)
		{
			if (mode_ != null)
				return major_+"."+minor_+"."+subminor_+" ("+mode_+")";
			else
				return major_+"."+minor_+"."+subminor_;
		}
		return "0.0.0";
	}
	
	private long getVersionNb()
	{
		return major_*1000*1000+minor_*1000+subminor_;
	}
	
	public boolean compare(Version v)
	{
		if (v.getVersionNb() > this.getVersionNb()) return true;
		return false;
	}
	
	public static Version parse(boolean work, String workMessage)
	{
		String buildTime = "";
		String st =  "dev";
		try {
			st = Manifests.read("version");
			String buildTimeTmp = Manifests.read("Build-Time");
			String y = buildTimeTmp.substring(0,4);
			String m = buildTimeTmp.substring(4,6);
			String d = buildTimeTmp.substring(6,8);
			String hh = buildTimeTmp.substring(8,10);
			String mm = buildTimeTmp.substring(10,12);
			String ss = buildTimeTmp.substring(12,14);
			buildTime = d+"/"+m+"/"+y+" "+hh+":"+mm+":"+ss;
		} catch (java.lang.IllegalArgumentException | IndexOutOfBoundsException e) {
		}
		
		Version v = new Version();
		v.complete_ = false;
		String parts[] = st.split("\\.");
		if (parts.length == 3)
		{
			v.major_ = Integer.valueOf(parts[0]);
			v.minor_ = Integer.valueOf(parts[1]);
			
			String subminor = parts[2];
			
			v.mode_ = System.getenv("ANALYTISCOUT_MODE");
			
			int index = subminor.indexOf("-");
			if (index > 0) {
				String subminorNb = subminor.substring(0, index);
				v.subminor_ = Integer.valueOf(subminorNb);
				v.mode_ = subminor.substring(index+1);
			}
			else {
				v.subminor_ = Integer.valueOf(subminor);
			}
			v.complete_ = true;
		}
		v.buildDate_ = buildTime;
		v.active_ = work;
		v.workMessage_ = workMessage;
		return v;
	}

}