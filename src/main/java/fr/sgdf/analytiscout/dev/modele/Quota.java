package fr.sgdf.analytiscout.dev.modele;

import fr.sgdf.analytiscout.dev.QuotaSF;
import fr.sgdf.analytiscout.dev.QuotasSFStatus;
import fr.sgdf.intranetapi.modeles.adherent.Branche;

public class Quota {

    public Unite unite;

    public Integer quotaDir;
    public int quotaDir_theorique;
    public int quotaDir_effectif;

    public Integer nbPSC1;
    public int nbPSC1_theorique;
    public int nbPSC1_effectif;

    public int jeunes;
    public int chefs;

    public QuotasSFStatus quotaSF;

    public Quota(Unite unite) {
        this.unite = unite;
        quotaDir_theorique = 1;
        nbPSC1_theorique = 1;
    }

    public void complete(Unite unite) {
        quotaDir_effectif += unite.dirsfqnonq;
        nbPSC1_effectif += unite.psc1;

        chefs += unite.chefs;
        jeunes += unite.jeunes;
    }

    public int getCodestructure() {
        return this.unite.codeStructure;
    }

    public void finalise() {
        quotaDir_effectif = Math.min(1, quotaDir_effectif);
        nbPSC1 = nbPSC1_effectif - nbPSC1_theorique;

        if (unite.branche != Branche.FARFADET && unite.branche != Branche.AUDACE) {
            quotaDir = quotaDir_effectif - quotaDir_theorique;
            quotaSF =
                QuotaSF.calcul(
                    jeunes,
                    chefs,
                    unite.branche,
                    quotaDir_effectif,
                    unite.qualifs,
                    unite.stagiaires,
                    chefs - (quotaDir_effectif + unite.qualifs + unite.stagiaires)
                );
        }

    }
}
