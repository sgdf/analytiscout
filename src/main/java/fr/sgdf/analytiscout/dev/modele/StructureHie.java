package fr.sgdf.analytiscout.dev.modele;

import java.util.ArrayList;
import java.util.List;

import fr.sgdf.intranetapi.modeles.structure.Structure;

public class StructureHie {
	public List<StructureHie> children = new ArrayList<StructureHie>();
	public String label;
	public String branche;
	public Structure data;
	public int codeStructure;
	public boolean selectable = true;
	
	public int getCodeStructure() {
		return codeStructure;
	}
	
	public StructureHie(Structure structure) {
		label = structure.nomStructure;
		data = structure;
		branche = structure.branche.name();
		codeStructure = structure.codeStructure;
	}
	
	public StructureHie(StructureHie structure) {
		label = structure.label;
		branche = structure.branche;
		data = structure.data;
		codeStructure = structure.codeStructure;
	}

	@Override
	public String toString() {
		return label;
	}
}
