package fr.sgdf.analytiscout.dev;

import java.util.TreeSet;

import fr.sgdf.analytiscout.dev.modele.Diplome;

public class DiplomeSet extends TreeSet<Diplome> {
	public Diplome getT(String nom) {
		for (Diplome value : this) {
			if (value.tType.compareTo(nom) == 0)
				return value;
		}
		return null;
	}
	
	public Diplome get(String nom) {
		for (Diplome value : this) {
			if (value.type.compareTo(nom) == 0)
				return value;
		}
		return null;
	}
}
