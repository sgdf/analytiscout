package fr.sgdf.analytiscout.dev;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import fr.sgdf.analytiscout.config.ApplicationProperties;
import fr.sgdf.analytiscout.config.ApplicationProperties.Prometheus;
import fr.sgdf.analytiscout.security.AuthoritiesConstants;

public class UsersMgr implements UserDetailsManager {

	private PasswordEncoder encrypter = new BCryptPasswordEncoder();
	final private ApplicationProperties appProperties;
	
	public UsersMgr(ApplicationProperties appProperties) {
		this.appProperties = appProperties;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Prometheus prometheus = appProperties.getPrometheus();
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        User user = null;
        if (prometheus != null && prometheus.getUtilisateur() != null && username.compareTo(prometheus.getUtilisateur()) == 0) {
            authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN));
        	user = new User(username, encrypter.encode(prometheus.getMotDePasse()), true, true,true,true, authorities);
        }
        else {
        	authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.USER));
        	user = new User(username, encrypter.encode(username), true, true,true,true, authorities);
        }
		return user;
	}

	@Override
	public void createUser(UserDetails user) {
	}

	@Override
	public void updateUser(UserDetails user) {
	}

	@Override
	public void deleteUser(String username) {
	}

	@Override
	public void changePassword(String oldPassword, String newPassword) {
	}

	@Override
	public boolean userExists(String username) {
		return true;
	}
}
