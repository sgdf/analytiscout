package fr.sgdf.analytiscout.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Analytiscout.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link tech.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    // jhipster-needle-application-properties-property

    // jhipster-needle-application-properties-property-getter

    // jhipster-needle-application-properties-property-class
    
	private final RedisCache cacheutilisateurs = new RedisCache();
	private final Intranet intranet = new Intranet();
	private final Work work = new Work();
	private final Prometheus prometheus = new Prometheus();
	
	public RedisCache getCacheUtilisateurs() {
        return cacheutilisateurs;
    }
	
	public Intranet getIntranet() {
		return intranet;
	}
	
	public Work getWork() {
		return work;
	}
	
	public Prometheus getPrometheus() {
		return prometheus;
	}
	
	public static class Prometheus {
		private String utilisateur = null;
		private String motDePasse = null;
		
		public String getUtilisateur() {
			return utilisateur;
		}
		public void setUtilisateur(String utilisateur) {
			this.utilisateur = utilisateur;
		}
		public String getMotDePasse() {
			return motDePasse;
		}
		public void setMotDePasse(String motDePasse) {
			this.motDePasse = motDePasse;
		}
		
	}
	
	public static class Work {
		private boolean active = false;
		private String workMessage= null;
		
		public boolean isActive() {
			return active;
		}
		
		public void setActive(boolean active) {
			this.active = active;
		}
		
		public String getWorkMessage() {
			return workMessage;
		}
		
		public void setWorkMessage(String workMessage) {
			this.workMessage = workMessage;
		}
	}
	
	public static class Intranet {
		private String clientId = "";
		private String urlAuth;
		private String url;
		private boolean anonymiser = false;
		private boolean filtrage = true;
		private String clientIdKeycloak = null;
		private String secretKeycloak = null;
		private String realms = null;
		
		public String getUrl() {
			return url;
		}
		
		public void setUrl(String url) {
			this.url = url;
		}
		
		public String getClientIdKeycloak() {
			return clientIdKeycloak;
		}
		
		public void setClientIdKeycloak(String clientIdKeycloak) {
			this.clientIdKeycloak = clientIdKeycloak;
		}
		
		public boolean isAnonymiser() {
			return anonymiser;
		}
		
		public void setAnonymiser(boolean anonymiser) {
			this.anonymiser = anonymiser;
		}
		
		public boolean isFiltrage() {
			return filtrage;
		}
		
		public void setFiltrage(boolean filtrage) {
			this.filtrage = filtrage;
		}
		
		public String getClientId() {
			return clientId;
		}
		
		public void setClientId(String clientId) {
			this.clientId = clientId;
		}

		public String getRealms() {
			return realms;
		}

		public void setRealms(String realms) {
			this.realms = realms;
		}

		public String getUrlAuth() {
			return urlAuth;
		}

		public void setUrlAuth(String urlAuth) {
			this.urlAuth = urlAuth;
		}

		public String getSecretKeycloak() {
			return secretKeycloak;
		}

		public void setSecretKeycloak(String secretKeycloak) {
			this.secretKeycloak = secretKeycloak;
		}
	}
	
	public static class RedisCache {
		private String host = "";
		private String port = "6379";
		private String auth = "";
		private String expire = "180";
		private String expires = "86400";
        private Integer db = 0;
		
		public void setHost(String host) {
			this.host = host;
		}

		public void setPort(String port) {
			this.port = port;
		}

		public void setAuth(String auth) {
			this.auth = auth;
		}

		public void setExpire(String expire) {
			this.expire = expire;
		}

		public void setExpires(String expires) {
			this.expires = expires;
		}
		
		public String getHost() {
			return host;
		}
		
		public String getPort() {
			return port;
		}
		
		public String getAuth() {
			return auth;
		}
		
		public String getExpire() {
			return expire;
		}
		
		public String getExpires() {
			return expires;
		}

		public Integer getDb() {
			return db;
		}

		public void setDb(Integer db) {
			this.db = db;
		}
	}
}
